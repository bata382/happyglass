#include "TrapezoidL.h"
#include <cmath>

TrapezL::TrapezL(const Point2DL& topLeftCorner, const Point2DL& topRightCorner, const Point2DL& bottomLeftCorner, const Point2DL& bottomRightCorner):
	m_topLeftCorner(topLeftCorner),m_topRightCorner(topRightCorner),m_bottomLeftCorner(bottomLeftCorner),m_bottomRightCorner(bottomRightCorner)
{
}

//Baza mare
float TrapezL::CalculateBase1()
{
	float Base1 = m_bottomLeftCorner.GetDistance(m_bottomRightCorner);

	return Base1;
}

//Baza mica
float TrapezL::CalculateBase2()
{
	float Base2 = m_topLeftCorner.GetDistance(m_topRightCorner);

	return Base2;
}

float TrapezL::CalculateHeight()
{
	//T.Pitagora
	//oneSide=ipotenuza
	//smallPortion=cateta
	//height=cealalta cateta
	float oneSide = m_topRightCorner.GetDistance(m_bottomRightCorner);

	float Base1 = CalculateBase1();
	float Base2 = CalculateBase2();
	float smallPortion = (Base1 - Base2) / 2;
	
	//Teorema: oneSide^2=height^2 + smallPortion^2
	//-> height 
	float height = sqrt(oneSide * oneSide - smallPortion * smallPortion);

	return height;
}

float TrapezL::getArea()
{
	float Base1 = CalculateBase1();
	float Base2 = CalculateBase2();
	float height = CalculateHeight();

	float area = (Base1 + Base2) / 2 * height;

	return area;
	
}
