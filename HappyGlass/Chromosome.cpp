#include "Chromosome.h"
#include "GaFourthLevel.h"

Chromosome::Chromosome(std::vector<std::pair<uint16_t, uint16_t>>& points):
	m_chromosome(points)
{
	m_fitness = CalculateFitness();
}

std::vector<std::pair<uint16_t, uint16_t>> Chromosome::GetChromosome() const
{
	return m_chromosome;
}

void Chromosome::SetChromosome(const std::vector<std::pair<uint16_t, uint16_t>> chromosome)
{
	if (!m_chromosome.empty())
		m_chromosome.clear();

	m_chromosome = chromosome;
	m_fitness = CalculateFitness();
}

float Chromosome::CalculateFitness()
{
	GaFourthLevel aiFourthLevel;

	aiFourthLevel.Run(m_chromosome);
	float areaParticles = aiFourthLevel.GetAreaParticles();

	return areaParticles / m_chromosome.size();
}

bool operator<(const Chromosome& firstChromosome, const Chromosome& secondChromosome)
{
	return firstChromosome.m_fitness < secondChromosome.m_fitness;
}
