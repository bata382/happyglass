#include "ThirdLevel.h"
#include "Globals.h"
#include "..\Logging\Logger.h"
#include "ThirdLevelScene.h"
#include "TextG.h"

#include <fstream>
#include <thread>

ThirdLevel::ThirdLevel() :
	m_window(sf::VideoMode(Globals::WindowElements::width, Globals::WindowElements::height),
		"Third Level",
		sf::Style::Default,
		sf::ContextSettings(32, 0, 0, 3, 0)),
	m_lines(),
	m_scene(m_window)
{
}

void ThirdLevel::Run()
{
	sf::Texture background;
	sf::Sprite spriteBackground;
	InitializeWindow(background, spriteBackground);

	std::chrono::system_clock::time_point currentFrame = std::chrono::system_clock::now();
	std::chrono::system_clock::time_point lastFrame = std::chrono::system_clock::now();

	ThirdLevelScene scene(m_window);

	/*Level loop*/
	while (m_window.isOpen())
	{
		/*Limit framerate*/
		CalculateFramerate(currentFrame, lastFrame);
		/*Set framerate for physic world*/
		m_scene.GetWorld().Step(1 / 60.f, 8, 3);
		m_scene.Start();
		uint16_t size = 0;
		sf::Event event;
		DrawCurvesG& line = m_scene.GetLine();

		/*Make an action based one type event*/
		while (m_window.pollEvent(event))
			MakeAction(event, line);

		m_lines.UpdateLinePosition();

		if (line.m_locked && line.GetLastMousePos() != sf::Mouse::getPosition())
		{
			std::vector<RectangleG>& shapes = m_scene.GetShapes();

			sf::Vector2f positions;
			positions = sf::Vector2f(sf::Mouse::getPosition().x - m_window.getPosition().x + line.GetBorderOffset().x,
				sf::Mouse::getPosition().y - m_window.getPosition().y + line.GetBorderOffset().y);

			line.AddPoint(sf::Vertex(positions, line.GetCurrentColor()), shapes);

			std::string memorizePositionX = std::to_string(static_cast<int>(positions.x));
			std::string memorizePositionY = std::to_string(static_cast<int>(positions.y));

			std::ofstream logFile("log.log", std::ios::app);
			Logger logger(logFile, Logger::Level::Info);
			logger.Log("X:" + memorizePositionX + "  " + "Y:" + memorizePositionY, Logger::Level::Info);

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && line.GetIsOkToDraw())
				m_scene.ModifyLevelDrainBar(line.GetSizeLine(), size);
		}

		m_window.clear();
		m_window.draw(spriteBackground);
		m_scene.Draw();
		m_lines.Draw(m_window);

		m_window.display();
	}
}

sf::RenderWindow& ThirdLevel::GetWindow()
{
	return m_window;
}

void ThirdLevel::InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground)
{
	if (!background.loadFromFile("mathPage.png"))
		throw "Image can't load!\n";

	spriteBackground.scale(sf::Vector2f(4.2f, 4.2f));
	spriteBackground.setTexture(background);
}

void ThirdLevel::CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
	std::chrono::system_clock::time_point& lastFrame)
{
	currentFrame = std::chrono::system_clock::now();
	std::chrono::duration<double, std::milli> workTime = currentFrame - lastFrame;
	uint16_t fps = Globals::Timer::frames;
	if (workTime.count() < fps)
	{
		std::chrono::duration<double, std::milli> delta(fps - workTime.count());
		auto deltaDuration = std::chrono::duration_cast<std::chrono::milliseconds>(delta);
		std::this_thread::sleep_for(std::chrono::milliseconds(deltaDuration.count()));
		++m_scene.m_numberFrame;
	}

	lastFrame = std::chrono::system_clock::now();
	std::chrono::duration<double, std::milli> sleepTime = lastFrame - currentFrame;
}

void ThirdLevel::MakeAction(sf::Event& event, DrawCurvesG& line)
{
	switch (event.type)
	{
	case sf::Event::Closed:
	{
		m_window.close();
		break;
	}
	case sf::Event::MouseButtonPressed:
	{
		sf::Vector2f pos = m_window.mapPixelToCoords(sf::Mouse::getPosition(m_window));

		if (pos.y > Globals::HelpButton::y + Globals::HelpButton::width)
		{
			line.m_locked = true;
			/*Let water fall.*/
			m_scene.GetWater().m_okFalling = true;
		}
		else
		{
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				if (m_scene.IsInsideBackButton(pos) == true)
					m_window.close();
				if (m_scene.IsInsideHelpButton(pos) == true)
					m_scene.m_drawHintLine = true;
				if (m_scene.IsInsideRestartButton(pos) == true)
				{
					m_window.close();
					ThirdLevel restart;
					restart.Run();
				}
			}
		}
		break;
	}
	case sf::Event::MouseButtonReleased:
	{
		line.AddFinalPoint();
		std::vector<sf::VertexArray> test = line.GetLinePoints();
		if (!test.empty())
			m_lines.CreateLine(m_scene.GetWorld(), test);
		break;
	}
	default:
	{
		if (line.m_locked == false)
			m_scene.m_click = true;
		break;
	}
	}
}