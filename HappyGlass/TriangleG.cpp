#include "TriangleG.h"

TriangleG::TriangleG(uint16_t radius,
	const sf::Color& color,
	const sf::Vector2f& position,
	float angle)
{
	m_triangle.setRadius(radius);
	m_triangle.setPointCount(3);
	m_triangle.rotate(angle);
	m_triangle.setFillColor(color);
	m_triangle.setOutlineColor(sf::Color::Black);
	m_triangle.setPosition(position.x, position.y);
}

const sf::CircleShape& TriangleG::GetTriangle() const
{
	return m_triangle;
}