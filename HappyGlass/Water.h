#pragma once
#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>

#include <random>
#include <vector>

class Water
{
public:
	Water();
	Water(const Water& water) = default;
	Water(Water&& water) = default;
	Water& operator=(const Water& water) = default;
	Water& operator=(Water&& water) = default;
	~Water() = default;

	bool m_okFalling;

private:
	uint16_t m_nrDroplets;
	float m_spRate;
	float m_dt;
	std::vector<b2Body*> m_particles;

	/*Random generator.*/
	std::random_device m_dev;
	std::mt19937 m_rng;
	std::uniform_int_distribution<std::mt19937::result_type> m_noise;

public:
	void StartFirstLevel(sf::Clock& dtClock, b2World& world);
	void StartSecondLevel(sf::Clock& dtClock, b2World& world);
	void StartThirdLevel(sf::Clock& dtClock, b2World& world);
	void StartFourthLevel(sf::Clock& dtClock, b2World& world);
	void CreateCircle(b2World& world, float xCoord, float yCoord);
	std::vector<b2Body*> GetParticles() const;
};