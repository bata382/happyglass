#pragma once
#include "Chromosome.h"

#include <string>

class Population
{
public:
	Population(uint8_t level);
	Population(const Population& point) = default;
	Population(Population&& point) = default;
	Population& operator=(const Population& point) = default;
	Population& operator=(Population&& point) = default;
	~Population() = default;

	void Evolution();

private:
	void Read(const std::string& level);

	void Selection(uint8_t numberOfParents);
	void Merge(Chromosome& firstChromosome, Chromosome& secondChromosome);
	void Mutation();

private:
	std::vector<Chromosome> m_population;
	Chromosome m_bestChromosome;
};

