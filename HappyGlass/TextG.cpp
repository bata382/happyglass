#include "Globals.h"
#include "TextG.h"

#include <sstream>

TextG::TextG()
{
	if (!m_font.loadFromFile("arial.ttf"))
		throw "Error loading file!\n";
}

sf::Text TextG::GenerateBackButtonText()
{
	sf::Text textBackButton;

	textBackButton.setFont(m_font);
	textBackButton.setString("Back");
	textBackButton.setCharacterSize(30);
	textBackButton.setOutlineColor(sf::Color::Black);
	textBackButton.setStyle(sf::Text::Style::Bold);
	textBackButton.setFillColor(sf::Color::Color(56, 217, 235));
	textBackButton.setOutlineThickness(5);
	textBackButton.setPosition(928, 55);

	return textBackButton;
}

sf::Text TextG::GenerateFinishText()
{
	sf::Text textFinish;

	textFinish.setFont(m_font);
	textFinish.setString("FINISH");
	textFinish.setCharacterSize(60);
	textFinish.setOutlineColor(sf::Color::Black);
	textFinish.setStyle(sf::Text::Style::Bold);
	textFinish.setFillColor(sf::Color::Color(56, 217, 235));
	textFinish.setOutlineThickness(5);
	textFinish.setPosition(425, 250);

	return textFinish;
}

sf::Text TextG::GenerateGameOverText()
{
	sf::Text textGameOver;

	textGameOver.setFont(m_font);
	textGameOver.setString("GAME OVER");
	textGameOver.setCharacterSize(60);
	textGameOver.setOutlineColor(sf::Color::Black);
	textGameOver.setStyle(sf::Text::Style::Bold);
	textGameOver.setFillColor(sf::Color::Color(56, 217, 235));
	textGameOver.setOutlineThickness(5);
	textGameOver.setPosition(425, 250);

	return textGameOver;
}

sf::Text TextG::GenerateHelpButtonText()
{
	sf::Text textHelpButton;

	textHelpButton.setFont(m_font);
	textHelpButton.setString("?");
	textHelpButton.setCharacterSize(27);
	textHelpButton.setStyle(sf::Text::Style::Bold);
	textHelpButton.setFillColor(sf::Color::Red);
	textHelpButton.setPosition(Globals::HelpButton::x + 7, Globals::HelpButton::y - 1);

	return textHelpButton;
}

sf::Text TextG::GenerateRestartButtonText()
{
	sf::Text textRestartButton;

	textRestartButton.setFont(m_font);
	textRestartButton.setString("R");
	textRestartButton.setCharacterSize(27);
	textRestartButton.setStyle(sf::Text::Style::Bold);
	textRestartButton.setFillColor(sf::Color::Red);
	textRestartButton.setPosition(Globals::RestartButton::x + 5, Globals::RestartButton::y - 1);

	return textRestartButton;
}

sf::Text TextG::GeneratePleaseDrawText(uint16_t count)
{
	sf::Text textPleaseDraw;

	if (count == 100)
	{
		textPleaseDraw.setFont(m_font);
		textPleaseDraw.setString("Please draw a line!");
		textPleaseDraw.setCharacterSize(30);
		textPleaseDraw.setOutlineColor(sf::Color::Black);
		textPleaseDraw.setStyle(sf::Text::Style::Bold);
		textPleaseDraw.setFillColor(sf::Color::Red);
		textPleaseDraw.setOutlineThickness(5);
		textPleaseDraw.setPosition(400, 250);
	}
	else
	{
		textPleaseDraw.setOutlineColor(sf::Color::Transparent);
		textPleaseDraw.setFillColor(sf::Color::Transparent);
	}

	return textPleaseDraw;
}

sf::Text TextG::GenerateNumberText(uint16_t count)
{
	sf::Text textNumber;
	std::ostringstream s;
	s << count;
	std::string i_as_string(s.str());

	textNumber.setFont(m_font);
	textNumber.setString(std::to_string(count));
	textNumber.setCharacterSize(30);
	textNumber.setOutlineColor(sf::Color::Black);
	textNumber.setStyle(sf::Text::Style::Bold);
	textNumber.setFillColor(sf::Color::Color(56, 217, 235));
	textNumber.setOutlineThickness(5);
	textNumber.setPosition(175, 65);

	return textNumber;
}

sf::Text TextG::GenerateGaText(const sf::Vector2f& position)
{
	return sf::Text();
}

std::vector<std::unique_ptr<sf::Text>> TextG::GenerateMenuTexts()
{
	std::vector<std::unique_ptr<sf::Text>> texts;
	sf::Text* text = new sf::Text();

	text->setFont(m_font);
	text->setString("Happy glass");
	text->setCharacterSize(120);
	text->setOutlineColor(sf::Color::Black);
	text->setStyle(sf::Text::Style::Bold);
	text->setFillColor(sf::Color::Color(56, 217, 235));
	text->setOutlineThickness(5);
	text->setPosition(200, 80);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	text->setFont(m_font);
	text->setString("First level");
	text->setCharacterSize(40);
	text->setOutlineColor(sf::Color::Black);
	text->setStyle(sf::Text::Style::Bold);
	text->setFillColor(sf::Color::Color(131, 76, 176));
	text->setOutlineThickness(5);
	text->setPosition(200, 370);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	text->setFont(m_font);
	text->setString("Second level");
	text->setCharacterSize(40);
	text->setOutlineColor(sf::Color::Black);
	text->setStyle(sf::Text::Style::Bold);
	text->setFillColor(sf::Color::Color(131, 76, 176));
	text->setOutlineThickness(5);
	text->setPosition(170, 500);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	text->setFont(m_font);
	text->setString("Third level");
	text->setCharacterSize(40);
	text->setOutlineColor(sf::Color::Black);
	text->setStyle(sf::Text::Style::Bold);
	text->setFillColor(sf::Color::Color(131, 76, 176));
	text->setOutlineThickness(5);
	text->setPosition(640, 370);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	text->setFont(m_font);
	text->setString("Fourth level");
	text->setCharacterSize(40);
	text->setOutlineColor(sf::Color::Black);
	text->setStyle(sf::Text::Style::Bold);
	text->setFillColor(sf::Color::Color(131, 76, 176));
	text->setOutlineThickness(5);
	text->setPosition(630, 500);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	text->setFont(m_font);
	text->setString("Instructions");
	text->setCharacterSize(30);
	text->setOutlineColor(sf::Color::Black);
	text->setStyle(sf::Text::Style::Bold);
	text->setFillColor(sf::Color::Color(56, 217, 235));
	text->setOutlineThickness(5);
	text->setPosition(440, 630);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	text->setFont(m_font);
	text->setString("GA");
	text->setCharacterSize(40);
	text->setOutlineColor(sf::Color::Black);
	text->setStyle(sf::Text::Style::Bold);
	text->setFillColor(sf::Color::Color(56, 217, 235));
	text->setOutlineThickness(5);
	text->setPosition(185 + Globals::FirstLevelButton::width, 370);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	text->setPosition(185 + Globals::FirstLevelButton::width, 500);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	text->setPosition(633 + Globals::ThirdLevelButton::width, 370);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	text->setPosition(633 + Globals::FourthLevelButton::width, 500);

	texts.emplace_back(std::make_unique<sf::Text>(*text));

	return texts;
}