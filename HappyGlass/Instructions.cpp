#include "Instructions.h"
#include "Globals.h"

Instructions::Instructions() :
	m_window(sf::VideoMode(Globals::WindowElements::width, Globals::WindowElements::height),
		"Instructions",
		sf::Style::Default,
		sf::ContextSettings(32, 0, 0, 3, 0))
{
}

void Instructions::Draw()
{
	TextG text;
	m_window.draw(text.GenerateBackButtonText());
}

void Instructions::Run()
{
	sf::Texture background;
	sf::Sprite spriteBackground;
	InitializeWindow(background, spriteBackground);

	while (m_window.isOpen())
	{
		sf::Event event;
		RectangleG backButton(sf::Vector2f(100, 50), sf::Vector2f(910, 50), sf::Color::Color(131, 76, 176), 0);
		while (m_window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				m_window.close();

			if (event.type == sf::Event::MouseButtonPressed)
			{
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					sf::Vector2f pos = m_window.mapPixelToCoords(sf::Mouse::getPosition(m_window));
					if (backButton.ContainsPoint(pos) == true)
						m_window.close();
				}
			}
		}

		m_window.clear();
		m_window.draw(spriteBackground);
		m_window.draw(backButton.GetRectangle());
		Draw();
		m_window.display();
	}
}

sf::RenderWindow& Instructions::GetWindow()
{
	return m_window;
}

void Instructions::InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground)
{
	if (!background.loadFromFile("textInstructions.png"))
		throw "Image can't load!\n";

	spriteBackground.scale(sf::Vector2f(0.93f, 1.19f));
	spriteBackground.setTexture(background);
}