#pragma once
#include <SFML/Graphics.hpp>

#include<vector>

class TextG
{
private:
	sf::Font m_font;

public:
	TextG();
	TextG(const TextG& text) = default;
	TextG(TextG&& text) = default;
	TextG& operator=(const TextG& text) = default;
	TextG& operator=(TextG&& text) = default;
	~TextG() = default;

	sf::Text GenerateBackButtonText();
	sf::Text GenerateFinishText();
	sf::Text GenerateGameOverText();
	sf::Text GenerateHelpButtonText();
	sf::Text GenerateRestartButtonText();
	sf::Text GeneratePleaseDrawText(uint16_t count);
	sf::Text GenerateNumberText(uint16_t count);
	sf::Text GenerateGaText(const sf::Vector2f& position);

	std::vector<std::unique_ptr<sf::Text>> GenerateMenuTexts();
};