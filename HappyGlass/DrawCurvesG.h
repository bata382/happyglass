#pragma once
#include "RectangleG.h"

#include <SFML/Graphics.hpp>

#include <iostream>
#include <vector>

class DrawCurvesG
{
public:
	DrawCurvesG();
	DrawCurvesG(const DrawCurvesG& line) = default;
	DrawCurvesG(DrawCurvesG&& line) = default;
	DrawCurvesG& operator=(const DrawCurvesG& line) = default;
	DrawCurvesG& operator=(DrawCurvesG&& line) = default;
	~DrawCurvesG() = default;

	std::vector<sf::VertexArray>& GetLinePoints();
	void AddPoint(const sf::Vertex& point, std::vector<RectangleG>& shapes);
	void AddFinalPoint();
	sf::Vector2i& GetLastMousePos();
	sf::Color& GetCurrentColor();
	sf::Vector2f& GetBorderOffset();
	bool GetIsOkToDraw() const;
	uint16_t GetSizeLine()const;
	void SetPointLines(const std::vector<std::pair<uint16_t, uint16_t>>& linePoints);

public:
	bool m_locked;

private:
	std::vector<sf::VertexArray> m_linePoints;
	sf::Vector2i m_lastMousePos;
	sf::Color m_currentColor;
	sf::Vector2f m_borderOffset;
	uint16_t m_linesNumber;
	bool m_isOkToDraw;
};

