#include "GaThirdLevel.h"
#include "Globals.h"
#include "FourthLevelScene.h"
#include "TextG.h"

#include <fstream>
#include <thread>

GaThirdLevel::GaThirdLevel() :
	m_window(sf::VideoMode(Globals::WindowElements::width, Globals::WindowElements::height),
		"Third Level Simulation",
		sf::Style::Default,
		sf::ContextSettings(32, 0, 0, 3, 0)),
	m_lines(),
	m_scene(m_window)
{
}

void GaThirdLevel::Run(const std::vector<std::pair<uint16_t, uint16_t>>& linePoints)
{
	sf::Texture background;
	sf::Sprite spriteBackground;
	InitializeWindow(background, spriteBackground);

	std::chrono::system_clock::time_point currentFrame = std::chrono::system_clock::now();
	std::chrono::system_clock::time_point lastFrame = std::chrono::system_clock::now();

	FourthLevelScene scene(m_window);

	/*Set our line to a predefinit line.*/
	DrawCurvesG line;
	line.SetPointLines(linePoints);
	m_scene.SetLine(line);
	line = m_scene.GetLine();

	m_scene.GetWater().m_okFalling = true;
	m_lines.CreateLine(m_scene.GetWorld(), line.GetLinePoints());

	/*Level loop*/
	while (m_window.isOpen())
	{
		/*Limit framerate*/
		CalculateFramerate(currentFrame, lastFrame);
		/*Set framerate for physic world*/
		m_scene.GetWorld().Step(1 / 60.f, 8, 3);
		m_scene.Start();
		uint16_t size = 0;
		sf::Event event;

		/*Make an action based one type event*/
		while (m_window.pollEvent(event))
			MakeAction(event, line);

		m_lines.UpdateLinePosition();
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			m_scene.ModifyLevelDrainBar(line.GetSizeLine(), size);

		m_window.clear();
		m_window.draw(spriteBackground);
		m_scene.Draw();
		m_lines.Draw(m_window);
		if (m_scene.IsWinning() == true)
		{
			std::cout << "Finish";
			m_window.close();
		}
		m_window.display();
	}
}

sf::RenderWindow& GaThirdLevel::GetWindow()
{
	return m_window;
}

float GaThirdLevel::GetAreaParticles()
{
	return m_scene.GetNoParticlesInGlass();
}

void GaThirdLevel::InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground)
{
	if (!background.loadFromFile("mathPage.png"))
	{
		throw "Image can't load!\n";
	}
	spriteBackground.scale(sf::Vector2f(4.2f, 4.2f));
	spriteBackground.setTexture(background);
}

void GaThirdLevel::CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
	std::chrono::system_clock::time_point& lastFrame)
{
	currentFrame = std::chrono::system_clock::now();
	std::chrono::duration<double, std::milli> workTime = currentFrame - lastFrame;
	uint16_t fps = 60;
	if (workTime.count() < fps)
	{
		std::chrono::duration<double, std::milli> delta(fps - workTime.count());
		auto deltaDuration = std::chrono::duration_cast<std::chrono::milliseconds>(delta);
		std::this_thread::sleep_for(std::chrono::milliseconds(deltaDuration.count()));
		++m_scene.m_numberFrame;
	}

	lastFrame = std::chrono::system_clock::now();
	std::chrono::duration<double, std::milli> sleepTime = lastFrame - currentFrame;
}

void GaThirdLevel::MakeAction(sf::Event& event, DrawCurvesG& line)
{
	switch (event.type)
	{
	case sf::Event::Closed:
	{
		m_window.close();
		break;
	}
	case sf::Event::MouseButtonPressed:
	{
		sf::Vector2f pos = m_window.mapPixelToCoords(sf::Mouse::getPosition(m_window));
		if (m_scene.IsInsideBackButton(pos) == true)
			m_window.close();
		break;
	}
	}
}