#pragma once
#include <Box2D/Box2D.h>
#include "SFML/Graphics.hpp"

#include <vector>

class DrawCurvesL
{
public:
	DrawCurvesL() = default;
	DrawCurvesL(const DrawCurvesL& line) = default;
	DrawCurvesL(DrawCurvesL&& line) = default;
	DrawCurvesL& operator=(const DrawCurvesL& line) = default;
	DrawCurvesL& operator=(DrawCurvesL&& line) = default;
	~DrawCurvesL() = default;
	
	void CreateLine(b2World& World, std::vector<sf::VertexArray> Curves);
	void CreateCurve(b2World& World, sf::VertexArray linePoints);
	void Draw(sf::RenderWindow& m_window);
	std::vector< b2Body*> GetCurves();
	void SetLinePosition();
	void UpdateLinePosition(size_t index);
	void UpdateLinePosition();

private:
	std::vector<b2Body*> m_curves;
	std::vector<sf::ConvexShape> m_parts;
	std::vector<b2PolygonShape> m_shapes;
};

