#include "DrawCurvesL.h"
#include "Globals.h"

#include <iostream>

void DrawCurvesL::CreateLine(b2World& world, std::vector<sf::VertexArray> curves)
{
	uint16_t index = 0;

	while (index < curves.size())
	{
		CreateCurve(world, curves[index]);

		curves.erase(curves.begin() + index, curves.begin() + index + 1);
		index--;
		index++;
	}

}

void DrawCurvesL::CreateCurve(b2World& world, sf::VertexArray linePoints)
{
	b2BodyDef bdef;
	bdef.type = b2_dynamicBody;
	//bdef.type = b2_staticBody;
	bdef.position = b2Vec2(0, 0);
	b2FixtureDef fixtureDef;
	fixtureDef.density = 200.f;
	fixtureDef.friction = 50000000;
	fixtureDef.restitution = 0.00000001f;
	b2Body* body = world.CreateBody(&bdef);

	if (linePoints.getVertexCount() >= 1)
	{
		b2Vec2* vert = new b2Vec2[linePoints.getVertexCount()];
		float x4 = 0;
		float y4 = 0;
		float h = 0.5f;

		for (size_t index = 0; index < linePoints.getVertexCount() - 1; ++index)
		{
			float x1 = linePoints[index].position.x / Globals::UnitOfMeasure::scale;
			float y1 = linePoints[index].position.y / Globals::UnitOfMeasure::scale;

			float x2 = linePoints[index + 1].position.x / Globals::UnitOfMeasure::scale;
			float y2 = linePoints[index + 1].position.y / Globals::UnitOfMeasure::scale;

			float l = (h * h) / (1 + ((x2 - x1) * (x2 - x1)) / ((y2 - y1) * (y2 - y1)));  // l = a*a, m = b*b
			float m = (h * h) - l;

			float x3 = l >= 0 ? (float)sqrt(l) + x2 : (float)sqrt(-l) + x2;
			float y3 = m >= 0 ? -(float)sqrt(m) + y2 : -(float)sqrt(-m) + y2;

			//for the first fixture, (x4,y4) needs to be calculated explicitly
			if (x4 == 0 && y4 == 0) {
				x4 = l >= 0 ? (float)sqrt(l) + x1 : (float)sqrt(-l) + x1;
				y4 = m >= 0 ? -(float)sqrt(m) + y1 : -(float)sqrt(-m) + y1;
			}

			vert[0].Set(x1, y1);
			vert[1].Set(x2, y2);
			vert[2].Set(x3, y3);
			vert[3].Set(x4, y4);

			b2PolygonShape shape;
			shape.Set(vert, 4);
			fixtureDef.shape = &shape;
			//shape.SetAsBox(1, 1);
			body->CreateFixture(&fixtureDef);
			body->SetAngularDamping(100);

			x4 = x3;
			y4 = y3;

			m_curves.push_back(body);
			m_shapes.push_back(shape);

			sf::ConvexShape polygon;
			polygon.setPointCount(4);
			polygon.setPoint(0, sf::Vector2f(shape.m_vertices[0].x * Globals::UnitOfMeasure::scale, -shape.m_vertices[0].y * Globals::UnitOfMeasure::scale));
			polygon.setPoint(1, sf::Vector2f(shape.m_vertices[1].x * Globals::UnitOfMeasure::scale, -shape.m_vertices[1].y * Globals::UnitOfMeasure::scale));
			polygon.setPoint(2, sf::Vector2f(shape.m_vertices[2].x * Globals::UnitOfMeasure::scale, -shape.m_vertices[2].y * Globals::UnitOfMeasure::scale));
			polygon.setPoint(3, sf::Vector2f(shape.m_vertices[3].x * Globals::UnitOfMeasure::scale, -shape.m_vertices[3].y * Globals::UnitOfMeasure::scale));
			polygon.setOutlineThickness(1);
			polygon.setOrigin(0, 0);
			polygon.setFillColor(sf::Color::Magenta);

			m_parts.push_back(polygon);
		}
	}
}

void DrawCurvesL::Draw(sf::RenderWindow& m_window)
{
	for (int i = 0; i < m_parts.size(); i++)
		m_window.draw(m_parts[i]);
}

std::vector<b2Body*> DrawCurvesL::GetCurves()
{
	return m_curves;
}

void DrawCurvesL::SetLinePosition()
{
	for (size_t index = 0; index < m_curves.size(); ++index)
	{
		sf::ConvexShape polygon;
		m_parts[index].setPoint(0, sf::Vector2f(m_shapes.at(index).m_vertices[0].x * Globals::UnitOfMeasure::scale, m_shapes.at(index).m_vertices[0].y * Globals::UnitOfMeasure::scale + 50));
		m_parts[index].setPoint(1, sf::Vector2f(m_shapes.at(index).m_vertices[1].x * Globals::UnitOfMeasure::scale, m_shapes.at(index).m_vertices[1].y * Globals::UnitOfMeasure::scale + 50));
		m_parts[index].setPoint(2, sf::Vector2f(m_shapes.at(index).m_vertices[2].x * Globals::UnitOfMeasure::scale, m_shapes.at(index).m_vertices[2].y * Globals::UnitOfMeasure::scale + 50));
		m_parts[index].setPoint(3, sf::Vector2f(m_shapes.at(index).m_vertices[3].x * Globals::UnitOfMeasure::scale, m_shapes.at(index).m_vertices[3].y * Globals::UnitOfMeasure::scale + 50));
	}

}

void DrawCurvesL::UpdateLinePosition()
{
	for (size_t index = 0; index < m_curves.size(); ++index)
	{

		m_parts[index].setPoint(0, sf::Vector2f(m_shapes.at(index).m_vertices[0].x * Globals::UnitOfMeasure::scale + 20, m_shapes.at(index).m_vertices[0].y * Globals::UnitOfMeasure::scale + 20));
		m_parts[index].setPoint(1, sf::Vector2f(m_shapes.at(index).m_vertices[1].x * Globals::UnitOfMeasure::scale + 20, m_shapes.at(index).m_vertices[1].y * Globals::UnitOfMeasure::scale + 20));
		m_parts[index].setPoint(2, sf::Vector2f(m_shapes.at(index).m_vertices[2].x * Globals::UnitOfMeasure::scale + 20, m_shapes.at(index).m_vertices[2].y * Globals::UnitOfMeasure::scale + 20));
		m_parts[index].setPoint(3, sf::Vector2f(m_shapes.at(index).m_vertices[3].x * Globals::UnitOfMeasure::scale + 20, m_shapes.at(index).m_vertices[3].y * Globals::UnitOfMeasure::scale + 20));

		m_parts[index].setRotation(180 / b2_pi * m_curves.at(index)->GetAngle());
		sf::Vector2f x = sf::Vector2f(m_curves.at(index)->GetPosition().x * Globals::UnitOfMeasure::scale, m_curves.at(index)->GetPosition().y * Globals::UnitOfMeasure::scale);
		m_parts[index].setPosition(x);
	}
}