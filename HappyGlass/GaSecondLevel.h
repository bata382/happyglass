#pragma once
#include <SFML/Graphics.hpp>

#include "DrawCurvesL.h"
#include "DrawCurvesG.h"
#include "ThirdLevelScene.h"
#include "RectangleG.h"

#include <chrono>

class GaSecondLevel
{
public:
	GaSecondLevel();
	GaSecondLevel(const GaSecondLevel& scene) = default;
	GaSecondLevel(GaSecondLevel&& scene) = default;
	GaSecondLevel& operator=(const GaSecondLevel& scene) = default;
	GaSecondLevel& operator=(GaSecondLevel&& scene) = default;
	~GaSecondLevel() = default;

	void Run(const std::vector<std::pair<uint16_t, uint16_t>>& linePoints);
	sf::RenderWindow& GetWindow();
	float GetAreaParticles();

private:
	void InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground);
	void CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
		std::chrono::system_clock::time_point& lastFrame);
	void MakeAction(sf::Event& event, DrawCurvesG& line);

private:
	ThirdLevelScene m_scene;
	DrawCurvesL m_lines;

	sf::RenderWindow  m_window;
};
