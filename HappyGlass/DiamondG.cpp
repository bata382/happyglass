#include "DiamondG.h"
#include "Globals.h"

DiamondG::DiamondG(const sf::Vector2f& position,
	uint16_t radius)
{
	m_diamond.setFillColor(sf::Color::Magenta);
	m_diamond.setPosition(position);
	m_diamond.setPointCount(4);
	m_diamond.setRadius(radius);
	m_diamond.setOutlineColor(sf::Color::Black);
	m_diamond.setOutlineThickness(5);
}

sf::CircleShape& DiamondG::GetDiamond()
{
	return m_diamond;
}

void TransparentDiamonds(std::vector<DiamondG>& diamonds, float count)
{
	if (count < Globals::DiamondsScore::twoStars)
		diamonds[2].GetDiamond().setFillColor(sf::Color::Transparent);
	if (count < Globals::DiamondsScore::oneStar)
		diamonds[1].GetDiamond().setFillColor(sf::Color::Transparent);
}
