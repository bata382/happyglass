#pragma once
#include <SFML/Graphics.hpp>

#include "DrawCurvesL.h"
#include "DrawCurvesG.h"
#include "ThirdLevelScene.h"
#include "RectangleG.h"

#include <chrono>

class GaThirdLevel
{
public:
	GaThirdLevel();
	GaThirdLevel(const GaThirdLevel& scene) = default;
	GaThirdLevel(GaThirdLevel&& scene) = default;
	GaThirdLevel& operator=(const GaThirdLevel& scene) = default;
	GaThirdLevel& operator=(GaThirdLevel&& scene) = default;
	~GaThirdLevel() = default;

	void Run(const std::vector<std::pair<uint16_t, uint16_t>>& linePoints);
	sf::RenderWindow& GetWindow();
	float GetAreaParticles();

private:
	void InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground);
	void CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
		std::chrono::system_clock::time_point& lastFrame);
	void MakeAction(sf::Event& event, DrawCurvesG& line);

private:
	ThirdLevelScene m_scene;
	DrawCurvesL m_lines;

	sf::RenderWindow  m_window;
};
