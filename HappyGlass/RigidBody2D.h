#pragma once
#include <SFML/Graphics.hpp>

#include <optional>
#include <vector>

class RigidBody2D
{
public:
	RigidBody2D();
	RigidBody2D(const sf::Vector2f& position);
	RigidBody2D(const RigidBody2D& body) = default;
	RigidBody2D(RigidBody2D&& body) = default;
	RigidBody2D& operator=(const RigidBody2D& body) = default;
	RigidBody2D& operator=(RigidBody2D&& body) = default;
	~RigidBody2D();

	void Movement(const float& dt, const float x, const float y);
	void Render(sf::RenderWindow& target);
	void Update(const float& dt);
	float GetWeight();
	float GetMovementSpeed();
	float GetGravitationalAcceleration();

protected:
	sf::CircleShape m_shape;
	float m_weight;
	float m_movementSpeed;
	float m_gravitationalAcceleration;
	bool m_staticBody;
};