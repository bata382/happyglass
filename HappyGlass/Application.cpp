#include "Application.h"
#include "FirstLevel.h"
#include "FourthLevel.h"
#include "../Logging/Logger.h"
#include "Instructions.h"
#include "Population.h"
#include "SecondLevel.h"
#include "ThirdLevel.h"

#include <fstream>
#include <thread>

Application* Application::m_application = nullptr;

Application::Application() :
	m_window(sf::VideoMode(1024, 768), 
	"Happy Glass", 
	sf::Style::Default, 
	sf::ContextSettings(32, 0, 0, 3, 0)),
	m_menu(Menu::Menu)
{
	Button button;
	m_buttons = button.GenerateButtonsForMenu();
}

Application::~Application()
{
	if (m_application != nullptr)
		delete m_application;
}

void Application::Run()
{
	sf::Texture background;
	sf::Sprite spriteBackground;
	/*Initialize window for application*/
	InitializeWindow(background, spriteBackground);

	std::chrono::system_clock::time_point currentFrame = std::chrono::system_clock::now();
	std::chrono::system_clock::time_point lastFrame = std::chrono::system_clock::now();

	/*Application loop*/
	while (m_window.isOpen())
	{
		CalculateFramerate(currentFrame, lastFrame);
		sf::Event event;

		while (m_window.pollEvent(event))
		{
			MakeAction(event);

			m_window.clear();
			m_window.draw(spriteBackground);
			Draw();
			m_window.display();
		}
	}
}

Application* Application::GetInstance()
{
	/*If we have an instance already defined we just return it. If we don't we create it*/
	if (m_application == nullptr)
		m_application = new Application();
	return m_application;
}

void Application::InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground)
{
	if (!background.loadFromFile("mathPage.png"))
	{
		throw "Image can't load!\n";
	}
	spriteBackground.scale(sf::Vector2f(4.2f, 4.2f));
	spriteBackground.setTexture(background);
}

void Application::MakeAction(const sf::Event& event)
{
	switch (event.type)
	{
	case sf::Event::Closed:
	{
		m_window.close();
		std::ofstream logFile("log.log", std::ios::app);
		Logger logger(logFile, Logger::Level::Info);
		logger.Log("Application was closed", Logger::Level::Info);
		break;
	}
	case sf::Event::MouseButtonPressed:
	{
		if (event.mouseButton.button == sf::Mouse::Left)
		{
			sf::Vector2f pos = m_window.mapPixelToCoords(sf::Mouse::getPosition(m_window));

			m_menu = (m_buttons[0].ContainsPoint(pos)) ? Menu::FirstLevel : m_menu;
			m_menu = (m_buttons[1].ContainsPoint(pos)) ? Menu::SecondLevel : m_menu;
			m_menu = (m_buttons[2].ContainsPoint(pos)) ? Menu::ThirdLevel : m_menu;
			m_menu = (m_buttons[3].ContainsPoint(pos)) ? Menu::FourthLevel : m_menu;
			m_menu = (m_buttons[4].ContainsPoint(pos)) ? Menu::Instructions : m_menu;
			m_menu = (m_buttons[5].ContainsPoint(pos)) ? Menu::GaFirstLevel : m_menu;
			m_menu = (m_buttons[6].ContainsPoint(pos)) ? Menu::GaSecondLevel : m_menu;
			m_menu = (m_buttons[7].ContainsPoint(pos)) ? Menu::GaThirdLevel : m_menu;
			m_menu = (m_buttons[8].ContainsPoint(pos)) ? Menu::GaFourthLevel : m_menu;
		}
		break;
	}
	}
}

void Application::Draw()
{
	switch (m_menu)
	{
	case Menu::Menu:
	{
		for (auto& button : m_buttons)
			m_window.draw(button.GetRectangle());

		/*Generate texts for principal menu*/
		TextG text;
		std::vector<std::unique_ptr<sf::Text>> texts = text.GenerateMenuTexts();

		for (auto& text : texts)
			m_window.draw(*text);
		break;
	}

	case Menu::FirstLevel:
	{
		std::ofstream logFile("log.log", std::ios::app);
		Logger logger(logFile, Logger::Level::Info);
		logger.Log("First level is running", Logger::Level::Info);

		/*Run first level*/
		FirstLevel firstLevel;
		while (firstLevel.GetWindow().isOpen())
			firstLevel.Run();
		m_menu = Menu::Menu;
		break;
	}

	case Menu::SecondLevel:
	{
		std::ofstream logFile("log.log", std::ios::app);
		Logger logger(logFile, Logger::Level::Info);
		logger.Log("Second level is running", Logger::Level::Info);

		/*Run second level*/
		SecondLevel secondLevel;
		while (secondLevel.GetWindow().isOpen())
			secondLevel.Run();
		m_menu = Menu::Menu;
		break;
	}

	case Menu::ThirdLevel:
	{
		std::ofstream logFile("log.log", std::ios::app);
		Logger logger(logFile, Logger::Level::Info);
		logger.Log("Third level is running", Logger::Level::Info);

		/*Run third level*/
		ThirdLevel thirdLevel;
		while (thirdLevel.GetWindow().isOpen())
			thirdLevel.Run();
		m_menu = Menu::Menu;
		break;
	}

	case Menu::FourthLevel:
	{
		std::ofstream logFile("log.log", std::ios::app);
		Logger logger(logFile, Logger::Level::Info);
		logger.Log("Fourth level is running", Logger::Level::Info);

		/*Run fourth level*/
		FourthLevel fourthLevel;
		while (fourthLevel.GetWindow().isOpen())
			fourthLevel.Run();
		m_menu = Menu::Menu;
		break;
	}

	case Menu::Instructions:
	{
		std::ofstream logFile("log.log", std::ios::app);
		Logger logger(logFile, Logger::Level::Info);
		logger.Log("Instructions were displayed", Logger::Level::Info);

		/*See instructions*/
		Instructions instructions;
		while (instructions.GetWindow().isOpen())
			instructions.Run();
		m_menu = Menu::Menu;
		break;
	}

	case Menu::GaFirstLevel:
	{
		Population pop(1);
		pop.Evolution();
		break;
	}

	case Menu::GaSecondLevel:
	{
		Population pop(2);
		pop.Evolution();
		break;
	}

	case Menu::GaThirdLevel:
	{
		Population pop(3);
		pop.Evolution();
		break;
	}

	case Menu::GaFourthLevel:
	{
		Population pop(4);
		pop.Evolution();
		break;
	}
	}
}

void Application::CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
	std::chrono::system_clock::time_point& lastFrame)
{
	currentFrame = std::chrono::system_clock::now();
	std::chrono::duration<double, std::milli> workTime = currentFrame - lastFrame;
	uint16_t fps = 60;
	if (workTime.count() < fps)
	{
		std::chrono::duration<double, std::milli> delta(fps - workTime.count());
		auto deltaDuration = std::chrono::duration_cast<std::chrono::milliseconds>(delta);
		std::this_thread::sleep_for(std::chrono::milliseconds(deltaDuration.count()));
	}

	lastFrame = std::chrono::system_clock::now();
	std::chrono::duration<double, std::milli> sleepTime = lastFrame - currentFrame;
	//std::cout << (workTime + sleepTime).count() << std::endl;
}

