#pragma once
#include <SFML/Graphics/RenderWindow.hpp>

#include "CircleG.h"
#include "DiamondG.h"
#include "DrawCurvesG.h"
#include "LevelEditor.h"
#include "RectangleG.h"
#include "Water.h"

class FourthLevelScene
{
public:
	FourthLevelScene() = default;
	FourthLevelScene(sf::RenderWindow& window,
		const std::vector<std::pair<uint16_t, uint16_t>>& linePoints);
	FourthLevelScene(sf::RenderWindow& window);
	FourthLevelScene(const FourthLevelScene& scene) = default;
	FourthLevelScene(FourthLevelScene&& scene) = default;
	FourthLevelScene& operator=(const FourthLevelScene& scene) = default;
	FourthLevelScene& operator=(FourthLevelScene&& scene) = default;
	~FourthLevelScene() = default;

	void Draw();
	void Start();
	void ModifyLevelDrainBar(uint16_t sizeLine, uint16_t& size);
	b2World& GetWorld();
	Water& GetWater();
	DrawCurvesG& GetLine();
	int GetNoParticlesInGlass() const;
	void SetLine(DrawCurvesG line);

	std::vector<RectangleG>& GetShapes();

	bool IsInsideBackButton(const sf::Vector2f& position) const;
	bool IsInsideHelpButton(const sf::Vector2f& position) const;
	bool IsInsideRestartButton(const sf::Vector2f& position) const;

	bool IsWinning() const;

public:
	bool m_drawHintLine;
	bool m_click;
	uint16_t m_numberFrame;

private:
	void Generate();
	void VerifyPositionParticles() const;

private:
	float m_count;
	mutable float m_areaParticles;

	LevelEditor m_levelEditor;
	DrawCurvesG m_line;
	std::vector<RectangleG> m_buttons;
	std::vector<RectangleG> m_shapes;
	std::vector<DiamondG> m_diamonds;
	std::vector<CircleG> m_waterParticles;

	sf::RenderWindow& m_window;
	sf::Clock dtClock;

	/** Prepare the Box2D world */
	b2Vec2 m_gravity;
	b2World m_world;
	Water m_water;
};