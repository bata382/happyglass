#pragma once
#include <SFML/Graphics/RenderWindow.hpp>

#include "CircleG.h"
#include "DiamondG.h"
#include "DrawCurvesG.h"
#include "LevelEditor.h"
#include "RectangleG.h"
#include "Water.h"

class SecondLevelScene
{
public:
	SecondLevelScene() = default;
	SecondLevelScene(sf::RenderWindow& window);
	SecondLevelScene(const SecondLevelScene& scene) = default;
	SecondLevelScene(SecondLevelScene&& scene) = default;
	SecondLevelScene& operator=(const SecondLevelScene& scene) = default;
	SecondLevelScene& operator=(SecondLevelScene&& scene) = default;
	~SecondLevelScene() = default;

	void Draw();
	void Start();
	void ModifyLevelDrainBar(uint16_t sizeLine, uint16_t& size);
	b2World& GetWorld();
	Water& GetWater();
	DrawCurvesG& GetLine();

	std::vector<RectangleG>& GetShapes();

	bool IsInsideBackButton(const sf::Vector2f& position) const;
	bool IsInsideHelpButton(const sf::Vector2f& position) const;
	bool IsInsideRestartButton(const sf::Vector2f& position) const;

public:
	bool m_drawHintLine;
	bool m_click;
	uint16_t m_numberFrame;

private:
	void Generate();
	bool IsWinning() const;
	void VerifyPositionParticles() const;

private:
	uint16_t m_countSeconds;
	float m_count;
	mutable float m_areaParticles;

	LevelEditor m_levelEditor;
	DrawCurvesG m_line;
	std::vector<RectangleG> m_shapes;
	std::vector<DiamondG> m_diamonds;
	std::vector<CircleG> m_waterParticles;

	sf::RenderWindow& m_window;
	std::vector<RectangleG> m_buttons;
	sf::Clock m_dtClock;

	/** Prepare the Box2D world */
	b2Vec2 m_gravity;
	b2World m_world;
	Water m_water;
};