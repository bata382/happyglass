#include "Population.h"
#include "GaFourthLevel.h"
#include "Globals.h"

#include <fstream>
#include <iostream>

/*Generam un nr random pt o regula de productie*/
uint16_t GenerateRandomNumber(uint16_t minLimit, uint16_t maxLimit)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dist(minLimit, maxLimit - 1);
	return dist(mt);
}

Population::Population(uint8_t level)
{
	if (level == 1)
		Read("First");
	if (level == 2)
		Read("Second");
	if (level == 3)
		Read("Third");
	if (level == 4)
		Read("Fourth");
}

void Population::Evolution()
{
	size_t index = 0;

	m_bestChromosome = m_population[0];

	while (index < Globals::GeneticAlgorithm::epoch)
	{
		/*Select top candidates.*/
		Selection(Globals::GeneticAlgorithm::individsSelected);
		m_bestChromosome = m_population[0];

		/*Merge canditates.*/
		for (size_t index = 0; index < m_population.size(); index += 2)
			Merge(m_population[index], m_population[index + 1]);

		/*Mutation.*/
		Mutation();

		/*Start AiFourthLevel.*/
		for (const auto& individ : m_population)
		{
			GaFourthLevel fourthLevel;
			fourthLevel.Run(individ.GetChromosome());
		}
	}
}

void Population::Selection(uint8_t numberOfParents)
{
	/*Sort population after fitness cost.*/
	std::sort(m_population.begin(), m_population.end(),
		[](const Chromosome& first, const Chromosome& second)
	{
		return !(first < second);
	}
	);

	/*Erase uneccesary individuals*/
	m_population.erase(m_population.begin() + numberOfParents, m_population.end());
}

void Population::Merge(Chromosome& firstChromosome, Chromosome& secondChromosome)
{
	uint16_t firstSize = firstChromosome.GetChromosome().size();
	uint16_t secondSize = secondChromosome.GetChromosome().size();
	uint16_t min = firstSize < secondSize ? firstSize : secondSize;

	uint8_t crossoverPosition = GenerateRandomNumber(0, min);

	/*Get crossovered elements from first chromosome.*/
	std::vector<std::pair<uint16_t, uint16_t>> firstVec = firstChromosome.GetChromosome();
	std::vector<std::pair<uint16_t, uint16_t>> crossoveredPointsFromFirst;
	std::copy(firstVec.begin() + crossoverPosition,
		firstVec.end(),
		std::back_inserter(crossoveredPointsFromFirst));
	/*Erased crossovered elements.*/
	firstVec.erase(firstVec.begin() + crossoverPosition, firstVec.end());

	/*Get crossovered elements from second chromosome.*/
	std::vector<std::pair<uint16_t, uint16_t>> secondVec = secondChromosome.GetChromosome();
	std::vector<std::pair<uint16_t, uint16_t>> crossoveredPointsFromSecond;
	std::copy(secondVec.begin() + crossoverPosition,
		secondVec.end(),
		std::back_inserter(crossoveredPointsFromSecond));
	/*Erased crossovered elements.*/
	secondVec.erase(secondVec.begin() + crossoverPosition, secondVec.end());

	/*Swap crossovered elements.*/
	crossoveredPointsFromFirst.swap(crossoveredPointsFromSecond);

	/*Reconstruct chromosomes*/
	for (const auto& element : crossoveredPointsFromFirst)
		firstVec.emplace_back(element);

	for (const auto& element : crossoveredPointsFromSecond)
		secondVec.emplace_back(element);

	firstChromosome.SetChromosome(firstVec);
	secondChromosome.SetChromosome(secondVec);
}

void Population::Mutation()
{
	for (size_t indexChromosome = 0; indexChromosome < m_population.size(); ++indexChromosome)
	{
		uint8_t numberOfMutations = GenerateRandomNumber(0, m_population.size());
		size_t index = 0;
		std::vector<std::pair<uint16_t, uint16_t>> chromosome = m_population[indexChromosome].GetChromosome();
		while (index < numberOfMutations)
		{
			uint8_t position = GenerateRandomNumber(1, chromosome.size() - 1);

			uint16_t minFirst = chromosome[position - 1].first;
			uint16_t maxFirst = chromosome[position + 1].first;

			if (minFirst > maxFirst)
				std::swap(minFirst, maxFirst);

			uint16_t minSecond = chromosome[position - 1].second;
			uint16_t maxSecond = chromosome[position + 1].second;

			if (minSecond > maxSecond)
				std::swap(minSecond, maxSecond);

			chromosome[position] = std::make_pair<uint16_t, uint16_t>(
				GenerateRandomNumber(minFirst, maxFirst),
				GenerateRandomNumber(minSecond, maxSecond));
			++index;
		}
		m_population[indexChromosome].SetChromosome(chromosome);
	}
}

void Population::Read(const std::string& level)
{
	std::ifstream file("log.log");

	while (!file.eof())
	{
		bool okToParse = false;
		std::string intermediarString;

		/*Ajungem la linia nivelului pt care vrem sa adaugam punctele.*/
		while (getline(file, intermediarString)) {
			if (intermediarString.find(level) != std::string::npos)
				break;
		}

		/*Incepem sa parcurgem liniile astfle incat sa adaugam punctele.*/
		std::vector<std::pair<uint16_t, uint16_t>> intermediarPoints;

		while (getline(file, intermediarString))
		{
			/*Mai trebuie adaugat pt scor.*/
			if (intermediarString.find("X:") == std::string::npos && intermediarString.find("Y:") == std::string::npos)
				break;
			else
			{
				size_t index = 0;
				while (index < intermediarString.size())
				{
					uint16_t x;
					uint16_t y;
					std::string numberString;
					if (intermediarString[index] == 'X')
					{
						size_t indexNumber = index + 2;
						while (intermediarString[indexNumber] != ' ')
						{
							numberString += intermediarString[indexNumber];
							++indexNumber;
						}
						x = stoi(numberString);
						numberString.clear();
						index = indexNumber - 1;
					}
					if (intermediarString[index] == 'Y')
					{
						size_t indexNumber = index + 2;
						while (indexNumber < intermediarString.size() && intermediarString[indexNumber] != ' ')
						{
							numberString += intermediarString[indexNumber];
							++indexNumber;
						}
						y = stoi(numberString);
						intermediarPoints.emplace_back(x, y);
					}
					++index;
				}
			}
		}
		if (!intermediarPoints.empty())
			m_population.emplace_back(intermediarPoints);
	}
}
