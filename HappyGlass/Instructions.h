#pragma once
#include "RectangleG.h"
#include "TextG.h"

class Instructions
{
public:
	Instructions();
	Instructions(const Instructions& instruction) = default;
	Instructions(Instructions&& instruction) = default;
	Instructions& operator=(const Instructions& instruction) = default;
	Instructions& operator=(Instructions&& instruction) = default;
	~Instructions() = default;

	void Run();

	sf::RenderWindow& GetWindow();

private:
	void InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground);
	void Draw();

	sf::RenderWindow  m_window;
};

