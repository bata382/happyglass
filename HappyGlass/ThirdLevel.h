#pragma once
#include <SFML/Graphics.hpp>

#include "DrawCurvesL.h"
#include "DrawCurvesG.h"
#include "RectangleG.h"
#include "ThirdLevelScene.h"

#include <chrono>

class ThirdLevel
{
public:
	ThirdLevel();
	ThirdLevel(const ThirdLevel& scene) = default;
	ThirdLevel(ThirdLevel&& scene) = default;
	ThirdLevel& operator=(const ThirdLevel& scene) = default;
	ThirdLevel& operator=(ThirdLevel&& scene) = default;
	~ThirdLevel() = default;

	void Run();
	sf::RenderWindow& GetWindow();

private:
	void InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground);
	void CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
		std::chrono::system_clock::time_point& lastFrame);
	void MakeAction(sf::Event& event, DrawCurvesG& line);

private:
	sf::RenderWindow  m_window;
	ThirdLevelScene m_scene;
	DrawCurvesL m_lines;
};