#pragma once
#include "DrawCurvesL.h"
#include "FirstLevelScene.h"

#include <Box2D/Box2D.h>

#include <chrono>
#include <memory>
#include <vector>
#include <random>

class FirstLevel
{
public:
	FirstLevel();
	FirstLevel(const FirstLevel& scene) = default;
	FirstLevel(FirstLevel&& scene) = default;
	FirstLevel& operator=(const FirstLevel& scene) = default;
	FirstLevel& operator=(FirstLevel&& scene) = default;
	~FirstLevel() = default;

	void Run();

	sf::RenderWindow& GetWindow();
	std::vector<sf::Vector2f> GetMemorizeVector();

private:
	void InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground);
	void CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
		std::chrono::system_clock::time_point& lastFrame);
	void MakeAction(sf::Event& event, DrawCurvesG& line);
	void MemorizeIntoLogger();

private:
	DrawCurvesL m_lines;
	sf::RenderWindow  m_window;
	/*Scene for FirstLevel.*/
	FirstLevelScene m_scene;

	std::vector<sf::Vector2f> memorizeVector;
};