#include <cstdint>

namespace Globals
{
	namespace WindowElements
	{
		constexpr uint16_t width = 1024;
		constexpr uint16_t height = 768;
	};

	namespace Timer
	{
		constexpr uint16_t frames = 60;
		constexpr uint16_t secondsUnitlFinnish = 150;
	};

	namespace UnitOfMeasure
	{
		constexpr float scale = 30.f;
		constexpr float minimizeLevelBar = 0.835f;
	};

	namespace Glass
	{
		constexpr float percentGlassNeeded = 3 / 5.f;

		namespace FirstPlatform
		{
			constexpr uint16_t x = 735;
			constexpr uint16_t y = 353;
		};

		namespace SecondPlatform
		{
			constexpr uint16_t x = 530;
			constexpr uint16_t y = 253;
		};

		namespace ThirdPlatform
		{
			constexpr uint16_t x = 673;
			constexpr uint16_t y = 353;
		};

		namespace FourthPlatform
		{
			constexpr uint16_t x = 630;
			constexpr uint16_t y = 353;
		};
	}

	namespace Circle
	{
		constexpr float PI = 3.14f;
		constexpr float radius = 0.5f;
	}

	namespace BackButton
	{
		constexpr float x = 910.f;
		constexpr float y = 50.f;
		constexpr float width = 100.f;
		constexpr float height = 50.f;
	}

	namespace HelpButton
	{
		constexpr float x = 910.f;
		constexpr float y = 130.f;
		constexpr float width = 30.f;
		constexpr float height = 30.f;
	}

	namespace RestartButton
	{
		constexpr float x = 980.f;
		constexpr float y = 130.f;
		constexpr float width = 30.f;
		constexpr float height = 30.f;
	}

	namespace FirstLevelButton
	{
		constexpr float x = 150.f;
		constexpr float y = 360.f;
		constexpr float width = 280.f;
		constexpr float height = 75.f;
	}

	namespace SecondLevelButton
	{
		constexpr float x = 150.f;
		constexpr float y = 490.f;
		constexpr float width = 280.f;
		constexpr float height = 75.f;
	}

	namespace ThirdLevelButton
	{
		constexpr float x = 600.f;
		constexpr float y = 360.f;
		constexpr float width = 280.f;
		constexpr float height = 75.f;
	}

	namespace FourthLevelButton
	{
		constexpr float x = 600.f;
		constexpr float y = 490.f;
		constexpr float width = 280.f;
		constexpr float height = 75.f;
	}

	namespace GeneticAlgorithm
	{
		constexpr uint8_t individsSelected = 4;
		constexpr uint8_t epoch = 10;
	}

	namespace DiamondsScore
	{
		constexpr uint8_t oneStar = 34;
		constexpr uint8_t twoStars = 67;
	}
};