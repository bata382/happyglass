#include "RectangleG.h"

RectangleG::RectangleG(const sf::Vector2f& size,
	const sf::Vector2f& position,
	const sf::Color& color,
	float angle)
{
	m_rectangle.setSize(size);
	m_rectangle.setOutlineColor(sf::Color::Black);
	m_rectangle.setOutlineThickness(5);
	m_rectangle.setPosition(position);
	m_rectangle.setFillColor(color);
	m_rectangle.rotate(angle);
}

sf::RectangleShape& RectangleG::GetRectangle()
{
	return m_rectangle;
}

float area(int x1, int y1, int x2, int y2,
	int x3, int y3)
{
	return abs((x1 * (y2 - y3) + x2 * (y3 - y1) +
		x3 * (y1 - y2)) / 2.0);
}

bool check(int x1, int y1, int x2, int y2, int x3,
	int y3, int x4, int y4, int x, int y)
{
	float A = area(x1, y1, x2, y2, x3, y3) +
		area(x1, y1, x4, y4, x3, y3);

	float A1 = area(x, y, x1, y1, x2, y2);
	float A2 = area(x, y, x2, y2, x3, y3);
	float A3 = area(x, y, x3, y3, x4, y4);
	float A4 = area(x, y, x1, y1, x4, y4);

	return (A == A1 + A2 + A3 + A4);
}

bool RectangleG::ContainsPoint(const sf::Vector2f& point) const
{
	sf::Vector2f rectangleDimension = m_rectangle.getSize();
	sf::Vector2f rectanglePosition = m_rectangle.getPosition();

	if (check(m_rectangle.getPoint(0).x + rectanglePosition.x,
		m_rectangle.getPoint(0).y + rectanglePosition.y,
		m_rectangle.getPoint(1).x + rectanglePosition.x,
		m_rectangle.getPoint(1).y + rectanglePosition.y,
		m_rectangle.getPoint(2).x + rectanglePosition.x,
		m_rectangle.getPoint(2).y + rectanglePosition.y,
		m_rectangle.getPoint(3).x + rectanglePosition.x,
		m_rectangle.getPoint(3).y + rectanglePosition.y,
		point.x,
		point.y)
		== true)
		return true;

	return false;
}