#include "Water.h"
#include "Globals.h"

Water::Water() :
	m_rng(m_dev()),
	m_noise(0, 15),
	m_spRate(0.09f),
	m_nrDroplets(60),
	m_okFalling(false)
{
}

void Water::StartFirstLevel(sf::Clock& dtClock, b2World& world)
{
	if (m_okFalling == true)
	{
		if (m_nrDroplets > 0)
		{
			m_dt = dtClock.getElapsedTime().asSeconds();
			if (m_dt >= m_spRate)
			{
				CreateCircle(world, 80, 160);
				dtClock.restart();
				m_nrDroplets--;
			}
		}
	}
}

void Water::StartSecondLevel(sf::Clock& dtClock, b2World& world)
{
	if (m_okFalling == true)
	{
		if (m_nrDroplets > 0)
		{
			m_dt = dtClock.getElapsedTime().asSeconds();
			if (m_dt >= m_spRate)
			{
				CreateCircle(world, 430, 220);
				dtClock.restart();
				m_nrDroplets--;
			}
		}
	}
}

void Water::StartThirdLevel(sf::Clock& dtClock, b2World& world)
{
	if (m_okFalling == true)
	{
		if (m_nrDroplets > 0)
		{
			m_dt = dtClock.getElapsedTime().asSeconds();
			if (m_dt >= m_spRate)
			{
				CreateCircle(world, 430, 220);
				dtClock.restart();
				m_nrDroplets--;
			}
		}
	}
}

void Water::StartFourthLevel(sf::Clock& dtClock, b2World& world)
{
	if (m_okFalling == true)
	{
		if (m_nrDroplets > 0)
		{
			m_dt = dtClock.getElapsedTime().asSeconds();
			if (m_dt >= m_spRate)
			{
				CreateCircle(world, 330, 220);
				dtClock.restart();
				m_nrDroplets--;
			}
		}
	}
}

void Water::CreateCircle(b2World& world, float xCoord, float yCoord)
{
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2((xCoord + m_noise(m_rng)) / Globals::UnitOfMeasure::scale,
		yCoord / Globals::UnitOfMeasure::scale);
	bodyDef.type = b2_dynamicBody;

	b2Body* body = world.CreateBody(&bodyDef);

	b2CircleShape circleShape;
	circleShape.m_p.Set(0, 0);
	circleShape.m_radius = 0.3f;
	b2FixtureDef fixtureDef;
	fixtureDef.density = 200.f;
	fixtureDef.friction = 5;
	fixtureDef.restitution = 0.001f;
	fixtureDef.shape = &circleShape;
	body->CreateFixture(&fixtureDef);
	m_particles.push_back(body);
}

std::vector<b2Body*> Water::GetParticles() const
{
	return m_particles;
}