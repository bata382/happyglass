#include "Hints.h"
#include <vector>
#include <SFML/Graphics.hpp>
#include <fstream>
#include <vector>

Hints::Hints()
{
}

void Hints::HelperDrawHint(const std::string& filename)
{
	std::vector<sf::CircleShape> circleShape;
	std::ifstream readFile(filename);
	readFile >> m_numberOfHintCoords;
	for (size_t index = 0; index < m_numberOfHintCoords; index++)
	{
		sf::CircleShape circle(3);
		circle.setFillColor(sf::Color::Red);
		circleShape.emplace_back(circle);
	}
	
	while (!readFile.eof())
	{
		sf::Vector2f coords;
		readFile >> coords.x;
		readFile >> coords.y;
		
		m_coords.emplace_back(coords);
	}
}

std::vector<sf::Vector2f> Hints::GetVectorCoords() const
{
	return m_coords;
}

uint16_t Hints::GetNumberOfHintCoords() const
{
	return m_numberOfHintCoords;
}