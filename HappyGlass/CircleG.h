#pragma once
#include "SFML/Graphics.hpp"

class CircleG
{
public:
	CircleG();
	CircleG(uint16_t radius,
		const sf::Color& color,
		const sf::Vector2f& position);
	CircleG(const CircleG& circle) = default;
	CircleG(CircleG&& circle) = default;
	CircleG& operator=(const CircleG& circle) = default;
	CircleG& operator=(CircleG&& circle) = default;
	~CircleG() = default;
	CircleG(const sf::Color& color);

	const sf::CircleShape& GetCircle() const;
	void SetPosition(const sf::Vector2f& position);

private:
	sf::CircleShape m_circle;
};