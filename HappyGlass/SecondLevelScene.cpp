#include "SecondLevelScene.h"
#include "Button.h"
#include "Globals.h"
#include "Hints.h"
#include "TextG.h"
#include "TriangleG.h"

SecondLevelScene::SecondLevelScene(sf::RenderWindow& window) :
	m_window(window),
	m_count(100),
	m_gravity(b2Vec2(8.f, 30.f)),
	m_world(m_gravity),
	m_water(),
	m_drawHintLine(false),
	m_levelEditor(),
	m_countSeconds(0)
{
	/*Generate phisic shapes.*/
	m_levelEditor.GenerateSecondLevel(m_world);

	/*Generate graphic shapes.*/
	Generate();

	/*Generate buttons*/
	Button button;
	m_buttons = button.GenerateButtonsForLevel();
}

void SecondLevelScene::Generate()
{
	RectangleG levelFill(sf::Vector2f(600, 30), sf::Vector2f(250, 68), sf::Color::Black);
	m_shapes.emplace_back(std::move(levelFill));

	RectangleG baseContainer(sf::Vector2f(70, 70), sf::Vector2f(420, 135), sf::Color::Black);
	m_shapes.emplace_back(std::move(baseContainer));

	RectangleG topContainer(sf::Vector2f(100, 10), sf::Vector2f(405, 215), sf::Color::Black);
	m_shapes.emplace_back(std::move(topContainer));

	RectangleG arrowBase(sf::Vector2f(7, 15), sf::Vector2f(450, 152), sf::Color::White);
	m_shapes.emplace_back(std::move(arrowBase));

	RectangleG rectangle(sf::Vector2f(150, 450), sf::Vector2f(220, 400), sf::Color::Magenta);
	m_shapes.emplace_back(std::move(rectangle));

	RectangleG supportGlass(sf::Vector2f(190, 10), sf::Vector2f(630, 653), sf::Color::Yellow);
	m_shapes.emplace_back(std::move(supportGlass));

	RectangleG leftLineGlass(sf::Vector2f(150, 7), sf::Vector2f(600, 515), sf::Color(128, 128, 128, 250), 60.f);
	m_shapes.emplace_back(leftLineGlass);

	RectangleG rightLineGlass(sf::Vector2f(150, 7), sf::Vector2f(740, 646), sf::Color(128, 128, 128, 250), 300.f);
	m_shapes.emplace_back(rightLineGlass);

	RectangleG middleLineGlass(sf::Vector2f(75, 7), sf::Vector2f(745, 649), sf::Color(128, 128, 128, 250), 180.f);
	m_shapes.emplace_back(middleLineGlass);

	RectangleG levelDrain(sf::Vector2f(600.0f, 30.0f), sf::Vector2f(250, 68), sf::Color::Color(145, 159, 171));
	m_shapes.emplace_back(std::move(levelDrain));

	DiamondG diamond1(sf::Vector2f(250, 15), 20);
	m_diamonds.emplace_back(std::move(diamond1));

	DiamondG diamond2(sf::Vector2f(300, 15), 20);
	m_diamonds.emplace_back(std::move(diamond2));

	DiamondG diamond3(sf::Vector2f(350, 15), 20);
	m_diamonds.emplace_back(std::move(diamond3));
}

void SecondLevelScene::Draw()
{
	for (size_t index = 0; index < m_buttons.size(); ++index)
		m_window.draw(m_buttons[index].GetRectangle());

	for (auto& shape : m_shapes)
		m_window.draw(shape.GetRectangle());

	for (auto& diamond : m_diamonds)
		m_window.draw(diamond.GetDiamond());

	TransparentDiamonds(m_diamonds, static_cast<uint16_t>(m_count));

	std::vector<sf::VertexArray> linePoints = m_line.GetLinePoints();
	for (size_t index = 0; index < linePoints.size(); ++index)
		m_window.draw(linePoints[index]);

	TriangleG arrow(20, sf::Color::White, sf::Vector2f(460, 150), 60);
	m_window.draw(arrow.GetTriangle());

	TextG text;
	m_window.draw(text.GenerateBackButtonText());
	m_window.draw(text.GenerateNumberText(static_cast<uint16_t>(m_count)));
	m_window.draw(text.GenerateHelpButtonText());
	m_window.draw(text.GenerateRestartButtonText());

	if (m_drawHintLine == true)
	{
		sf::CircleShape circle(3);
		circle.setFillColor(sf::Color::Red);

		Hints hint;
		hint.HelperDrawHint("coords2.txt");
		std::vector<sf::Vector2f> coords = hint.GetVectorCoords();

		for (size_t index = 0; index < coords.size(); index++)
		{
			circle.setPosition(coords[index]);
			m_window.draw(circle);
		}
	}

	std::vector<b2Body*> particles = m_water.GetParticles();
	uint16_t BodyCount = 0;

	for (size_t index = 0; index < particles.size(); ++index)
	{
		CircleG droplet;

		m_waterParticles.push_back(droplet);
		droplet.SetPosition(sf::Vector2f(Globals::UnitOfMeasure::scale * particles.at(index)->GetPosition().x, Globals::UnitOfMeasure::scale * particles.at(index)->GetPosition().y));
		++BodyCount;
		m_window.draw(droplet.GetCircle());
	}

	if (IsWinning() == true)
		m_window.draw(text.GenerateFinishText());
	else
		if (m_numberFrame >= 6* Globals::Timer::frames)
			m_window.draw(text.GenerateGameOverText());
		else
			if (m_click == true && m_numberFrame >= Globals::Timer::frames)
				m_window.draw(text.GeneratePleaseDrawText(m_count));
}

void SecondLevelScene::ModifyLevelDrainBar(uint16_t sizeLine, uint16_t& size)
{
	if (m_shapes[m_shapes.size() - 1].GetRectangle().getSize().x > 0.0f && (sizeLine - size) >= 5)
	{
		size = sizeLine;
		m_shapes[m_shapes.size() - 1].GetRectangle().
			setSize(sf::Vector2f(m_shapes[m_shapes.size() - 1].GetRectangle().getSize().x - 5.0f,
				m_shapes[m_shapes.size() - 1].GetRectangle().getSize().y));
		if (sizeLine > 5)
			m_count = m_count - 0.835f;
	}
}

std::vector<RectangleG>& SecondLevelScene::GetShapes()
{
	return m_shapes;
}

bool SecondLevelScene::IsInsideBackButton(const sf::Vector2f& position) const
{
	return m_buttons[2].ContainsPoint(position);
}

bool SecondLevelScene::IsInsideHelpButton(const sf::Vector2f& position) const
{
	return m_buttons[0].ContainsPoint(position);
}

bool SecondLevelScene::IsInsideRestartButton(const sf::Vector2f& position) const
{
	return m_buttons[1].ContainsPoint(position);
}


DrawCurvesG& SecondLevelScene::GetLine()
{
	return m_line;
}

bool SecondLevelScene::IsWinning() const
{
	VerifyPositionParticles();
	if (m_areaParticles > m_levelEditor.GetAreaGlass()* Globals::Glass::percentGlassNeeded)
		return true;
	return false;
}

b2World& SecondLevelScene::GetWorld()
{
	return m_world;
}

Water& SecondLevelScene::GetWater()
{
	return m_water;
}

void SecondLevelScene::Start()
{
	m_water.StartSecondLevel(m_dtClock, m_world);
}

void SecondLevelScene::VerifyPositionParticles() const
{
	constexpr float PI = Globals::Circle::PI;
	constexpr float radius = Globals::Circle::radius;
	uint16_t sum = 0;

	std::vector<b2Body*> particles = m_water.GetParticles();

	for (size_t index = 0; index < particles.size(); index++)
		if (particles[index]->GetPosition().x >= (2 + Globals::Glass::SecondPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y >= (8 + Globals::Glass::SecondPlatform::y / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().x >= (4.35 + Globals::Glass::SecondPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y <= (12 + Globals::Glass::SecondPlatform::y / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().x <= (6 + Globals::Glass::SecondPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y <= (12 + Globals::Glass::SecondPlatform::y / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().x <= (8.4 + Globals::Glass::SecondPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y >= (8 + Globals::Glass::SecondPlatform::y / Globals::UnitOfMeasure::scale))
			sum++;

	m_areaParticles = static_cast<float>(sum * PI * pow(radius, 2));
}