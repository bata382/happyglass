#include "CircleL.h"
#define _USE_MATH_DEFINES
#include <cmath>

CircleL::CircleL(int radius,
	const Point2DL& center) :
	m_radius(radius),
	m_center(center)
{
}

const float CircleL::GetArea() const
{
	return M_PI * (m_radius * m_radius);
}
