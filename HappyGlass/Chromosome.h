#pragma once
#include <vector>
#include <utility>

class Chromosome
{
public:
	Chromosome() = default;
	Chromosome(std::vector<std::pair<uint16_t, uint16_t>>& points);
	Chromosome(const Chromosome& point) = default;
	Chromosome(Chromosome&& point) = default;
	Chromosome& operator=(const Chromosome& point) = default;
	Chromosome& operator=(Chromosome&& point) = default;
	~Chromosome() = default;

	friend bool operator<(const Chromosome& firstChromosome, const Chromosome& secondChromosome);
	std::vector<std::pair<uint16_t, uint16_t>> GetChromosome() const;
	void SetChromosome(const std::vector<std::pair<uint16_t, uint16_t>> chromosome);

	float CalculateFitness();

private:
	std::vector<std::pair<uint16_t, uint16_t>> m_chromosome;
	float m_fitness;
};

