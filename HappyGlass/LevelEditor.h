#pragma once
#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

#include "TrapezoidL.h"

#include <vector>

class LevelEditor
{
public:
	LevelEditor() = default;
	LevelEditor(const LevelEditor& levelEditor) = default;
	LevelEditor(LevelEditor&& levelEditor) = default;
	LevelEditor& operator=(const LevelEditor& levelEditor) = default;
	LevelEditor& operator=(LevelEditor&& levelEditor) = default;
	~LevelEditor() = default;

	void GenerateFirstLevel(b2World& world);
	void GenerateSecondLevel(b2World& world);
	void GenerateThirdLevel(b2World& world);
	void GenerateFourthLevel(b2World& world);

	float GetAreaGlass() const;

private:
	b2Body* CreateBox2(b2World& world, float coordX, float coordY, float lenght, float height);
	void CreateBox(b2World& world, float coordX, float coordY, float lenght, float height);
	void CreateGlass(b2World& world, sf::Vector2f position);
	void CreateChain(b2World& world);

private:
	float m_areaGlass;
};