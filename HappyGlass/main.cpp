#include "Application.h"
#include "DrawCurvesG.h"
#include "../Logging/Logger.h"

#include <fstream>

int main()
{
	std::ofstream logFile("log.log",std::ios::app);
	Logger logger(logFile, Logger::Level::Info);
	logger.Log("Application started", Logger::Level::Info);

	Application* application = Application::GetInstance();
	application->Run();
	return 0;
}