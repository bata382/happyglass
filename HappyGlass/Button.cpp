#include "Button.h"
#include "Globals.h"

std::vector<RectangleG> Button::GenerateButtonsForLevel()
{
	std::vector<RectangleG> buttons;

	/*Help button*/
	buttons.emplace_back(
		sf::Vector2f(Globals::HelpButton::width, Globals::HelpButton::height),
		sf::Vector2f(Globals::HelpButton::x, Globals::HelpButton::y),
		sf::Color::Yellow, 0);

	/*Restart button*/
	buttons.emplace_back(
		sf::Vector2f(Globals::RestartButton::width, Globals::RestartButton::height),
		sf::Vector2f(Globals::RestartButton::x, Globals::RestartButton::y),
		sf::Color::Yellow, 0);

	/*BackButton*/
	buttons.emplace_back(
		sf::Vector2f(Globals::BackButton::width, Globals::BackButton::height),
		sf::Vector2f(Globals::BackButton::x, Globals::BackButton::y),
		sf::Color::Color(131, 76, 176), 0);

	return buttons;
}

std::vector<RectangleG> Button::GenerateButtonsForMenu()
{
	std::vector<RectangleG> buttons;

	/*First Level Button.*/
	buttons.emplace_back(RectangleG(sf::Vector2f(Globals::FirstLevelButton::width, Globals::FirstLevelButton::height),
		sf::Vector2f(Globals::FirstLevelButton::x, Globals::FirstLevelButton::y),
		sf::Color::Color(56, 217, 235), 0));

	/*Second Level Button.*/
	buttons.emplace_back(RectangleG(sf::Vector2f(Globals::SecondLevelButton::width, Globals::SecondLevelButton::height),
		sf::Vector2f(Globals::SecondLevelButton::x, Globals::SecondLevelButton::y),
		sf::Color::Color(56, 217, 235), 0));

	/*Third Level Button.*/
	buttons.emplace_back(RectangleG(sf::Vector2f(Globals::ThirdLevelButton::width, Globals::ThirdLevelButton::height),
		sf::Vector2f(Globals::ThirdLevelButton::x, Globals::ThirdLevelButton::y),
		sf::Color::Color(56, 217, 235), 0));

	/*Fourth Level Button.*/
	buttons.emplace_back(RectangleG(sf::Vector2f(Globals::FourthLevelButton::width, Globals::FourthLevelButton::height),
		sf::Vector2f(Globals::FourthLevelButton::x, Globals::FourthLevelButton::y),
		sf::Color::Color(56, 217, 235), 0));

	/*Instructions Button.*/
	buttons.emplace_back(RectangleG(sf::Vector2f(250, 60),
		sf::Vector2f(395, 620),
		sf::Color::Color(131, 76, 176), 0));

	/*GaFirstLevel*/
	buttons.emplace_back(
		RectangleG(sf::Vector2f(75, 75),
		sf::Vector2f(Globals::FirstLevelButton::x + Globals::FirstLevelButton::width + 25,
		Globals::FirstLevelButton::y),
		sf::Color::Color(131, 76, 176), 0)
	);

	/*GaSecondLevel*/
	buttons.emplace_back(
		RectangleG(sf::Vector2f(75, 75),
		sf::Vector2f(Globals::SecondLevelButton::x + Globals::SecondLevelButton::width + 25,
		Globals::SecondLevelButton::y),
		sf::Color::Color(131, 76, 176), 0)
	);

	/*GaThirdLevel*/
	buttons.emplace_back(
		RectangleG(sf::Vector2f(75, 75),
		sf::Vector2f(Globals::ThirdLevelButton::x + Globals::ThirdLevelButton::width + 25,
		Globals::ThirdLevelButton::y),
		sf::Color::Color(131, 76, 176), 0)
	);

	/*GaFourthLevel*/
	buttons.emplace_back(
		RectangleG(sf::Vector2f(75, 75),
		sf::Vector2f(Globals::FourthLevelButton::x + Globals::FourthLevelButton::width + 25,
		Globals::FourthLevelButton::y),
		sf::Color::Color(131, 76, 176), 0)
	);

	return buttons;
}
