#pragma once
#include <SFML/Graphics.hpp>
#include <vector>

class Hints
{
public:
	Hints();
	Hints(const Hints& hint) = default;
	Hints(Hints&& hint) = default;
	Hints& operator=(const Hints& hint) = default;
	Hints& operator=(Hints&& hint) = default;
	~Hints() = default;

	void HelperDrawHint(const std::string& filename);
	std::vector<sf::Vector2f> GetVectorCoords() const;
	uint16_t GetNumberOfHintCoords() const;

private:
	uint16_t m_numberOfHintCoords;
	std::vector<sf::Vector2f> m_coords;
};