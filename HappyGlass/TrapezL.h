#pragma once
#include "Point2DL.h"

class TrapezL
{
public:
	TrapezL(const Point2DL& topLeftCorner,
		const Point2DL& topRightCorner,
		const Point2DL& bottomLeftCorner,
		const Point2DL& bottomRightCorner);
	float CalculateBase1();
	float CalculateBase2();
	float CalculateHeight();
	float getArea();

private:
	Point2DL m_topLeftCorner;
	Point2DL m_topRightCorner;
	Point2DL m_bottomLeftCorner;
	Point2DL m_bottomRightCorner;


};

