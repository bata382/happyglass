#pragma once
#include "Point2DL.h"

class CircleL
{
public:
	CircleL(int radius, const Point2DL& center);

	const float GetArea() const;
	
private:
	int m_radius;
	Point2DL m_center;
	
};

