#pragma once
#include <SFML/Graphics.hpp>

class TriangleG
{
public:
	TriangleG(uint16_t radius,
		const sf::Color& color,
		const sf::Vector2f& position,
		float angle);
	TriangleG(const TriangleG& triangle) = default;
	TriangleG(TriangleG&& triangle) = default;
	TriangleG& operator=(const TriangleG& triangle) = default;
	TriangleG& operator=(TriangleG&& triangle) = default;
	~TriangleG() = default;

	const sf::CircleShape& GetTriangle() const;

private:
	sf::CircleShape m_triangle;
};