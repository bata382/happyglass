#pragma once
#include "CircleG.h"
#include "DiamondG.h"
#include "DrawCurvesG.h"
#include "LevelEditor.h"
#include "RectangleG.h"
#include "Water.h"

#include <Box2D/Box2D.h>
#include <SFML/Graphics/RenderWindow.hpp>

class FirstLevelScene
{
public:
	FirstLevelScene() = default;
	FirstLevelScene(sf::RenderWindow& window);
	FirstLevelScene(const FirstLevelScene& scene) = default;
	FirstLevelScene(FirstLevelScene&& scene) = default;
	FirstLevelScene& operator=(const FirstLevelScene& scene) = default;
	FirstLevelScene& operator=(FirstLevelScene&& scene) = default;
	~FirstLevelScene() = default;

	void Draw();
	void Start();
	void ModifyLevelDrainBar(uint16_t sizeLine, uint16_t& size);
	bool IsWinning() const;

	std::vector<RectangleG>& GetShapes();
	DrawCurvesG& GetLine();
	float GetScore() const;
	b2World& GetWorld();
	Water& GetWater();

	bool IsInsideBackButton(const sf::Vector2f& position) const;
	bool IsInsideHelpButton(const sf::Vector2f& position) const;
	bool IsInsideRestartButton(const sf::Vector2f& position) const;

public:
	bool m_drawHintLine;
	bool m_click;
	uint16_t m_numberFrame;

private:
	void VerifyPositionParticles() const;
	void Generate();

private:
	sf::RenderWindow& m_window;
	DrawCurvesG m_line;
	LevelEditor m_levelEditor;
	std::vector<RectangleG> m_buttons;
	std::vector<RectangleG> m_shapes;
	std::vector<DiamondG> m_diamonds;
	std::vector<CircleG> m_waterParticles;

	float m_count;
	mutable float m_areaParticles;

	sf::Clock m_dtClock;

	/** Prepare the Box2D world */
	b2Vec2 m_gravity;
	b2World m_world;
	Water m_water;
};

