#include "RigidBody2D.h"

RigidBody2D::RigidBody2D() :m_staticBody(false),
m_weight(1.f),
m_movementSpeed(100.f),
m_gravitationalAcceleration(10.f)
{
	m_shape.setRadius(30.f);
	m_shape.setPosition(85, 16);
	m_shape.setFillColor(sf::Color::Blue);
}

RigidBody2D::RigidBody2D(const sf::Vector2f& position)
{
	m_shape.setRadius(30.f);
	m_shape.setPosition(position.x, position.y);
	m_shape.setFillColor(sf::Color::Blue);
	m_staticBody = false;
	m_weight = 1.f;
	m_movementSpeed = 100.f;
	m_gravitationalAcceleration = 10.f;
}

RigidBody2D::~RigidBody2D()
{
}

void RigidBody2D::Movement(const float& dt, const float x, const float y)
{
	m_shape.move(x * m_movementSpeed * dt, y * m_movementSpeed * dt);
	system("cls");
}

void RigidBody2D::Render(sf::RenderWindow& target)
{
	target.draw(m_shape);
	system("cls");
}

void RigidBody2D::Update(const float& dt)
{
	if (m_staticBody == false)
		Movement(dt, 0.f, 1.f);
}

float RigidBody2D::GetWeight()
{
	return m_weight;
}

float RigidBody2D::GetMovementSpeed()
{
	return m_movementSpeed;
}

float RigidBody2D::GetGravitationalAcceleration()
{
	return m_gravitationalAcceleration;
}