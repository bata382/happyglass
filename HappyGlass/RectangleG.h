#pragma once
#include "SFML/Graphics.hpp"

class RectangleG
{
public:
	RectangleG() = default;
	RectangleG(const sf::Vector2f& size,
		const sf::Vector2f& position,
		const sf::Color& color = sf::Color::Black,
		float angle = 0.f);
	RectangleG(const RectangleG& rectangle) = default;
	RectangleG(RectangleG&& rectangle) = default;
	RectangleG& operator=(const RectangleG& rectangle) = default;
	RectangleG& operator=(RectangleG&& rectangle) = default;
	~RectangleG() = default;

	sf::RectangleShape& GetRectangle();
	bool ContainsPoint(const sf::Vector2f& position) const;

private:
	sf::RectangleShape m_rectangle;
};