#include "DrawCurvesG.h"

#include <ctime>

DrawCurvesG::DrawCurvesG() :
	m_linesNumber(0),
	m_locked(false),
	m_lastMousePos(0, 0),
	m_borderOffset(-5, -25),
	m_currentColor(sf::Color::Transparent),
	m_isOkToDraw(true)
{
	m_linePoints.push_back(sf::VertexArray());
	m_linePoints[0].setPrimitiveType(sf::LinesStrip);
}

std::vector<sf::VertexArray>& DrawCurvesG::GetLinePoints()
{
	return m_linePoints;
}

void DrawCurvesG::AddPoint(const sf::Vertex& point, std::vector<RectangleG>& shapes)
{
	for (auto& shape : shapes)
	{
		if (shape.ContainsPoint(sf::Vector2f(point.position.x, point.position.y)) == true)
		{
			m_isOkToDraw = false;
			break;
		}
	}
	if (m_isOkToDraw == true)
	{
		m_linePoints[m_linesNumber].append(point);
		m_lastMousePos = sf::Mouse::getPosition();
	}
}

void DrawCurvesG::AddFinalPoint()
{
	m_linesNumber++;
	m_linePoints.push_back(sf::VertexArray());
	m_linePoints[m_linesNumber].setPrimitiveType(sf::LinesStrip);
	m_locked = false;
}


sf::Vector2i& DrawCurvesG::GetLastMousePos()
{
	return m_lastMousePos;
}

sf::Color& DrawCurvesG::GetCurrentColor()
{
	return m_currentColor;
}

sf::Vector2f& DrawCurvesG::GetBorderOffset()
{
	return m_borderOffset;
}

bool DrawCurvesG::GetIsOkToDraw() const
{
	return m_isOkToDraw;
}

uint16_t DrawCurvesG::GetSizeLine() const
{
	return m_linePoints[m_linePoints.size() - 1].getVertexCount();
}

void DrawCurvesG::SetPointLines(const std::vector<std::pair<uint16_t, uint16_t>>& linePoints)
{
	sf::VertexArray vertexArray(sf::LinesStrip, linePoints.size());

	for (int index = 0; index < linePoints.size(); ++index)
	{
		vertexArray[index].position = sf::Vector2f(linePoints[index].first, linePoints[index].second);
		vertexArray[index].color = sf::Color::Transparent;
	}

	m_linePoints.emplace_back(vertexArray);
	AddFinalPoint();
}
