#pragma once
#include "Point2DL.h"

class RectangleL
{
public:
	RectangleL(const Point2DL& topLeftCorner,
		const Point2DL& topRightCorner,
		const Point2DL& bottomLeftCorner,
		const Point2DL& bottomRightCorner);

private:
	Point2DL m_topLeftCorner;
	Point2DL m_topRightCorner;
	Point2DL m_bottomLeftCorner;
	Point2DL m_bottomRightCorner;
};

