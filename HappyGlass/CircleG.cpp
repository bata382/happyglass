#include "CircleG.h"

CircleG::CircleG()
{
	m_circle.setRadius(20);
	m_circle.setFillColor(sf::Color::Blue);
	m_circle.setOutlineColor(sf::Color::Black);
	m_circle.setPosition(20, 20);
}

CircleG::CircleG(uint16_t radius,
	const sf::Color& color,
	const sf::Vector2f& position)
{
	m_circle.setRadius(radius);
	m_circle.setFillColor(color);
	m_circle.setOutlineColor(sf::Color::Black);
	m_circle.setPosition(position.x, position.y);
}

CircleG::CircleG(const sf::Color& color)
{
	m_circle.setRadius(20);
	m_circle.setFillColor(sf::Color::Black);
	m_circle.setOutlineColor(sf::Color::Black);
	m_circle.setPosition(20, 20);
}

const sf::CircleShape& CircleG::GetCircle() const
{
	return m_circle;
}

void CircleG::SetPosition(const sf::Vector2f& position)
{
	m_circle.setPosition(position);
}
