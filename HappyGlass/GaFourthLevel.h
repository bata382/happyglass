#pragma once
#include <SFML/Graphics.hpp>

#include "DrawCurvesL.h"
#include "DrawCurvesG.h"
#include "FourthLevelScene.h"
#include "RectangleG.h"

#include <chrono>

class GaFourthLevel
{
public:
	GaFourthLevel();
	GaFourthLevel(const GaFourthLevel& scene) = default;
	GaFourthLevel(GaFourthLevel&& scene) = default;
	GaFourthLevel& operator=(const GaFourthLevel& scene) = default;
	GaFourthLevel& operator=(GaFourthLevel&& scene) = default;
	~GaFourthLevel() = default;

	void Run(const std::vector<std::pair<uint16_t,uint16_t>>& linePoints);
	sf::RenderWindow& GetWindow();
	float GetAreaParticles();

private:
	void InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground);
	void CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
		std::chrono::system_clock::time_point& lastFrame);
	void MakeAction(sf::Event& event, DrawCurvesG& line);

private:
	FourthLevelScene m_scene;
	DrawCurvesL m_lines;

	sf::RenderWindow  m_window;
};

