#pragma once
#include "RectangleG.h"

#include <vector>

class Button
{
public:
	Button() = default;

	static std::vector<RectangleG> GenerateButtonsForLevel();
	static std::vector<RectangleG> GenerateButtonsForMenu();
};

