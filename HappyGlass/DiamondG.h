#pragma once
#include "SFML/Graphics.hpp"

#include <vector>

class DiamondG
{
public:
	DiamondG() = default;
	DiamondG(const sf::Vector2f& position, uint16_t radius);
	DiamondG(const DiamondG& diamond) = default;
	DiamondG(DiamondG&& diamond) = default;
	DiamondG& operator=(const DiamondG& diamond) = default;
	DiamondG& operator=(DiamondG&& diamond) = default;
	~DiamondG() = default;

	sf::CircleShape& GetDiamond();
	friend void TransparentDiamonds(std::vector<DiamondG>& diamonds, float count);

private:
	sf::CircleShape m_diamond;
};

