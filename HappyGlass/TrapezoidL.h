#pragma once
#include "Point2DL.h"

#include <cstdint>

class TrapezoidL
{
public:
	TrapezoidL(const Point2DL<uint16_t>& topLeftCorner,
		const Point2DL<uint16_t>& topRightCorner,
		const Point2DL<uint16_t>& bottomLeftCorner,
		const Point2DL<uint16_t>& bottomRightCorner);
	TrapezoidL(const TrapezoidL& trapezoid) = default;
	TrapezoidL(TrapezoidL&& trapezoid) = default;
	TrapezoidL& operator =(const TrapezoidL & trapezoid) = default;
	TrapezoidL& operator=(TrapezoidL&& trapezoid) = default;
	~TrapezoidL() = default;

	float CalculatePrincipalBase() const;
	float CalculateSecondaryBase() const;
	float CalculateHeight() const;
	float GetArea() const;

	const Point2DL<uint16_t>& GetTopLeftCorner() const;
	const Point2DL<uint16_t>& GetTopRightCorner() const;
	const Point2DL<uint16_t>& GetBottomLeftCorner() const;
	const Point2DL<uint16_t>& GetBottomRightCorner() const;

private:
	Point2DL<uint16_t> m_topLeftCorner;
	Point2DL<uint16_t> m_topRightCorner;
	Point2DL<uint16_t> m_bottomLeftCorner;
	Point2DL<uint16_t> m_bottomRightCorner;
};