#include "TrapezoidL.h"

#include <cmath>

TrapezoidL::TrapezoidL(const Point2DL<uint16_t>& topLeftCorner,
	const Point2DL<uint16_t>& topRightCorner,
	const Point2DL<uint16_t>& bottomLeftCorner,
	const Point2DL<uint16_t>& bottomRightCorner) :
	m_topLeftCorner(topLeftCorner),
	m_topRightCorner(topRightCorner),
	m_bottomLeftCorner(bottomLeftCorner),
	m_bottomRightCorner(bottomRightCorner)
{
}

float TrapezoidL::CalculatePrincipalBase() const
{
	float base1 = m_bottomLeftCorner.GetDistance(m_bottomRightCorner);
	return base1;
}

float TrapezoidL::CalculateSecondaryBase() const
{
	float base2 = m_topLeftCorner.GetDistance(m_topRightCorner);
	return base2;
}

float TrapezoidL::CalculateHeight() const
{
	float oneSide = m_topRightCorner.GetDistance(m_bottomRightCorner);
	float base1 = CalculatePrincipalBase();
	float base2 = CalculateSecondaryBase();
	float smallPortion = (base1 - base2) / 2;
	float height = sqrt(oneSide * oneSide - smallPortion * smallPortion);
	return height;
}

float TrapezoidL::GetArea() const
{
	float base1 = CalculatePrincipalBase();
	float base2 = CalculateSecondaryBase();
	float height = CalculateHeight();
	float area = (base1 + base2) / 2 * height;
	return area;
}

const Point2DL<uint16_t>& TrapezoidL::GetTopLeftCorner() const
{
	return m_topLeftCorner;
}

const Point2DL<uint16_t>& TrapezoidL::GetTopRightCorner() const
{
	return m_topRightCorner;
}

const Point2DL<uint16_t>& TrapezoidL::GetBottomLeftCorner() const
{
	return m_bottomLeftCorner;
}

const Point2DL<uint16_t>& TrapezoidL::GetBottomRightCorner() const
{
	return m_bottomRightCorner;
}