#include "LevelEditor.h"
#include "Globals.h"

void LevelEditor::CreateBox(b2World& World, float coordX, float coordY, float lenght, float height)
{
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(coordX / Globals::UnitOfMeasure::scale, coordY / Globals::UnitOfMeasure::scale);
	bodyDef.type = b2_staticBody;
	b2Body* body = World.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox((lenght / 2) / Globals::UnitOfMeasure::scale, (height / 2) / Globals::UnitOfMeasure::scale);
	b2FixtureDef FixtureDef;
	FixtureDef.density = 1.f;
	FixtureDef.friction = 5.f;
	FixtureDef.restitution = 0.1f;
	FixtureDef.shape = &shape;
	body->CreateFixture(&FixtureDef);
}

b2Body* LevelEditor::CreateBox2(b2World& world, float coordX, float coordY, float lenght, float height)
{
	b2BodyDef bodyDef;
	bodyDef.position = b2Vec2(coordX / Globals::UnitOfMeasure::scale, coordY / Globals::UnitOfMeasure::scale);
	bodyDef.type = b2_staticBody;
	b2Body* body = world.CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox((lenght / 2) / Globals::UnitOfMeasure::scale, (height / 2) / Globals::UnitOfMeasure::scale);
	b2FixtureDef fixtureDef;
	fixtureDef.density = 1.f;
	fixtureDef.friction = 0.1f;
	fixtureDef.restitution = 0.7f;
	fixtureDef.shape = &shape;
	body->CreateFixture(&fixtureDef);

	return  body;
}

void LevelEditor::GenerateFirstLevel(b2World& world)
{
	CreateBox(world, 80, 120, 20, 50);
	CreateBox(world, 100, 350, 400, 2);
	CreateBox(world, 490, 420, 300, 2);
	CreateGlass(world, sf::Vector2f(Globals::Glass::FirstPlatform::x, Globals::Glass::FirstPlatform::y));
}

void LevelEditor::GenerateSecondLevel(b2World& world)
{
	CreateBox(world, 430, 180, 20, 50);
	CreateBox(world, 275, 745, 170, 750);
	CreateGlass(world, sf::Vector2f(Globals::Glass::SecondPlatform::x, Globals::Glass::SecondPlatform::y));
}

void LevelEditor::GenerateThirdLevel(b2World& world)
{
	CreateBox(world, 430, 180, 20, 50);
	CreateBox(world, 345, 640, 15, 750);
	CreateBox(world, 440, 470, 210, 2);
	CreateGlass(world, sf::Vector2f(Globals::Glass::ThirdPlatform::x, Globals::Glass::ThirdPlatform::y));
}

void LevelEditor::GenerateFourthLevel(b2World& world)
{
	CreateBox(world, 330, 180, 20, 50);
	CreateBox(world, 275, 745, 50, 750);
	CreateBox(world, 415, 820, 50, 750);
	CreateGlass(world, sf::Vector2f(Globals::Glass::FourthPlatform::x, Globals::Glass::FourthPlatform::y));
}

float LevelEditor::GetAreaGlass() const
{
	return m_areaGlass;
}

void LevelEditor::CreateGlass(b2World& world, sf::Vector2f position)
{
	b2BodyDef bdg;
	bdg.type = b2_staticBody;
	b2Body* ground = world.CreateBody(&bdg);

	b2Vec2 vs[4];
	vs[0].Set(2.f + position.x / Globals::UnitOfMeasure::scale, 8.f + position.y / Globals::UnitOfMeasure::scale);
	vs[1].Set(4.35f + position.x / Globals::UnitOfMeasure::scale, 12.f + position.y / Globals::UnitOfMeasure::scale);
	vs[2].Set(6.f + position.x / Globals::UnitOfMeasure::scale, 12.f + position.y / Globals::UnitOfMeasure::scale);
	vs[3].Set(8.4f + position.x / Globals::UnitOfMeasure::scale, 8.f + position.y / Globals::UnitOfMeasure::scale);
	b2ChainShape shape;
	shape.CreateChain(vs, 4);
	b2FixtureDef FixtureDef;
	FixtureDef.density = 300000;
	FixtureDef.friction = 1000;
	FixtureDef.restitution = 0.001f;
	FixtureDef.shape = &shape;
	ground->CreateFixture(&FixtureDef);

	float base1, base2, height;

	Point2DL topLeftCorner(static_cast<uint16_t>(2 + position.x / Globals::UnitOfMeasure::scale),
		static_cast<uint16_t>(8 + position.y / Globals::UnitOfMeasure::scale));
	Point2DL bottomRightCorner(static_cast<uint16_t>(5 + position.x / Globals::UnitOfMeasure::scale),
		static_cast<uint16_t>(12 + position.y / Globals::UnitOfMeasure::scale));
	Point2DL topRightCorner(static_cast<uint16_t>(9 + position.x / Globals::UnitOfMeasure::scale),
		static_cast<uint16_t>(8 + position.y / Globals::UnitOfMeasure::scale));
	Point2DL bottomLeftCorner(static_cast<uint16_t>(6 + position.x / Globals::UnitOfMeasure::scale),
		static_cast<uint16_t>(12 + position.y / Globals::UnitOfMeasure::scale));

	TrapezoidL trapezoid(topLeftCorner, topRightCorner, bottomRightCorner, bottomLeftCorner);
	base1 = trapezoid.CalculatePrincipalBase();
	base2 = trapezoid.CalculateSecondaryBase();
	height = trapezoid.CalculateHeight();
	m_areaGlass = trapezoid.GetArea();
}

void LevelEditor::CreateChain(b2World& world)
{
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	b2Vec2 vs[4];
	vs[0].Set(33, 120);
	vs[1].Set(120, 120);
	vs[2].Set(600.0f, 500.0f);
	vs[3].Set(600, 430);
	b2ChainShape chain;
	chain.CreateChain(vs, 4);
}