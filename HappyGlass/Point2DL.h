#pragma once
#include <cmath>

template<class T>
class Point2DL
{
public:
	Point2DL(const T x, const T y) :m_x(x), m_y(y)
	{
	}
	Point2DL(const Point2DL<T>& point) = default;
	Point2DL(Point2DL<T>&& point) = default;
	Point2DL& operator=(const Point2DL<T>& point) = default;
	Point2DL& operator=(Point2DL<T>&& point) = default;
	~Point2DL() = default;

	const T GetX() const
	{
		return m_x;
	}

	const T GetY() const
	{
		return m_y;
	}

	const float GetDistance(const Point2DL<T>& point) const
	{
		return sqrt((point.m_x - m_x) * (point.m_x - m_x) + (point.m_y - m_y) * (point.m_y - m_y));
	}

private:
	T m_x;
	T m_y;
};