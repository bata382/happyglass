#include "FourthLevelScene.h"
#include "Globals.h"
#include "TextG.h"
#include "TriangleG.h"
#include "Button.h"
#include "Hints.h"

FourthLevelScene::FourthLevelScene(sf::RenderWindow& window,
	const std::vector<std::pair<uint16_t, uint16_t>>& linePoints) :
	m_window(window),
	m_count(100),
	m_gravity(b2Vec2(8.f, 30.f)),
	m_world(m_gravity),
	m_drawHintLine(false),
	m_water(),
	m_levelEditor()
{
	Generate();
	/*Generate phisic shapes.*/
	m_levelEditor.GenerateFourthLevel(m_world);

	/*Generate graphic shapes.*/
	Generate();

	/*Generate buttons.*/
	Button button;
	m_buttons = button.GenerateButtonsForLevel();
}

FourthLevelScene::FourthLevelScene(sf::RenderWindow& window) :
	m_window(window),
	m_count(100),
	m_gravity(b2Vec2(8.f, 30.f)),
	m_world(m_gravity),
	m_drawHintLine(false),
	m_water(),
	m_levelEditor()
{
	Generate();
	/*Generate phisic shapes.*/
	m_levelEditor.GenerateFourthLevel(m_world);

	/*Generate graphic shapes.*/
	Generate();

	/*Generate buttons.*/
	Button button;
	m_buttons = button.GenerateButtonsForLevel();
}

void FourthLevelScene::Generate()
{
	/*Level bar*/
	RectangleG levelFill(sf::Vector2f(600, 30), sf::Vector2f(250, 68), sf::Color::Black);
	m_shapes.emplace_back(std::move(levelFill));

	/*Container parts*/
	RectangleG baseContainer(sf::Vector2f(70, 70), sf::Vector2f(320, 135), sf::Color::Black);
	m_shapes.emplace_back(std::move(baseContainer));

	RectangleG topContainer(sf::Vector2f(100, 10), sf::Vector2f(305, 215), sf::Color::Black);
	m_shapes.emplace_back(std::move(topContainer));

	RectangleG arrowBase(sf::Vector2f(7, 15), sf::Vector2f(350, 152), sf::Color::White);
	m_shapes.emplace_back(std::move(arrowBase));

	/*Scene objects*/
	RectangleG rectangleUp(sf::Vector2f(30, 450), sf::Vector2f(280, 400), sf::Color::Magenta);
	m_shapes.emplace_back(std::move(rectangleUp));

	RectangleG rectangleDown(sf::Vector2f(30, 450), sf::Vector2f(420, 480), sf::Color::Magenta);
	m_shapes.emplace_back(std::move(rectangleDown));

	/*Glass support*/
	RectangleG supportGlass(sf::Vector2f(190, 10), sf::Vector2f(630 + 80, 653 + 100), sf::Color::Yellow);
	m_shapes.emplace_back(std::move(supportGlass));

	/*Glass*/
	RectangleG leftLineGlass(sf::Vector2f(150, 7), sf::Vector2f(600 + 100, 515 + 100), sf::Color(128, 128, 128, 250), 60.f);
	m_shapes.emplace_back(leftLineGlass);

	RectangleG rightLineGlass(sf::Vector2f(150, 7), sf::Vector2f(740 + 100, 646 + 100), sf::Color(128, 128, 128, 250), 300.f);
	m_shapes.emplace_back(rightLineGlass);

	RectangleG middleLineGlass(sf::Vector2f(75, 7), sf::Vector2f(745 + 100, 649 + 100), sf::Color(128, 128, 128, 250), 180.f);
	m_shapes.emplace_back(middleLineGlass);

	/*Level bar*/
	RectangleG levelDrain(sf::Vector2f(600.0f, 30.0f), sf::Vector2f(250, 68), sf::Color::Color(145, 159, 171));
	m_shapes.emplace_back(std::move(levelDrain));

	/*Diamonds object for level goal*/
	DiamondG diamond1(sf::Vector2f(250, 15), 20);
	m_diamonds.emplace_back(std::move(diamond1));

	DiamondG diamond2(sf::Vector2f(300, 15), 20);
	m_diamonds.emplace_back(std::move(diamond2));

	DiamondG diamond3(sf::Vector2f(350, 15), 20);
	m_diamonds.emplace_back(std::move(diamond3));
}

void FourthLevelScene::Draw()
{
	for (size_t index = 0; index < m_buttons.size(); ++index)
		m_window.draw(m_buttons[index].GetRectangle());

	for (auto& shape : m_shapes)
		m_window.draw(shape.GetRectangle());

	for (auto& diamond : m_diamonds)
		m_window.draw(diamond.GetDiamond());

	std::vector<sf::VertexArray> linePoints = m_line.GetLinePoints();
	for (size_t index = 0; index < linePoints.size(); index++)
		m_window.draw(linePoints[index]);

	TriangleG arrow(20, sf::Color::White, sf::Vector2f(360, 150), 60);
	m_window.draw(arrow.GetTriangle());

	TextG text;
	m_window.draw(text.GenerateBackButtonText());
	m_window.draw(text.GenerateNumberText(static_cast<uint16_t>(m_count)));

	if (m_drawHintLine == true)
	{
		sf::CircleShape circle(3);
		circle.setFillColor(sf::Color::Red);

		Hints hint;
		hint.HelperDrawHint("coords4.txt");
		std::vector<sf::Vector2f> coords = hint.GetVectorCoords();

		for (size_t index = 0; index < coords.size(); index++)
		{
			circle.setPosition(coords[index]);
			m_window.draw(circle);
		}
	}

	TransparentDiamonds(m_diamonds, static_cast<uint16_t>(m_count));

	std::vector<b2Body*> particles = m_water.GetParticles();
	uint16_t BodyCount = 0;

	for (size_t index = 0; index < particles.size(); ++index)
	{
		//graphics part
		CircleG droplet;

		m_waterParticles.push_back(droplet);
		droplet.SetPosition(sf::Vector2f(Globals::UnitOfMeasure::scale * particles.at(index)->GetPosition().x, Globals::UnitOfMeasure::scale * particles.at(index)->GetPosition().y));
		++BodyCount;
		m_window.draw(droplet.GetCircle());
	}

	if (IsWinning() == true)
		m_window.draw(text.GenerateFinishText());
	else
		if (m_numberFrame >= 6 * Globals::Timer::frames)
			m_window.draw(text.GenerateGameOverText());
		else
			if (m_click == true && m_numberFrame >= Globals::Timer::frames)
				m_window.draw(text.GeneratePleaseDrawText(m_count));

	m_window.draw(text.GenerateHelpButtonText());
	m_window.draw(text.GenerateRestartButtonText());
}

void FourthLevelScene::ModifyLevelDrainBar(uint16_t sizeLine, uint16_t& size)
{
	if (m_shapes[m_shapes.size() - 1].GetRectangle().getSize().x > 0.0f && (sizeLine - size) >= 5)
	{
		size = sizeLine;
		m_shapes[m_shapes.size() - 1].GetRectangle().
			setSize(sf::Vector2f(m_shapes[m_shapes.size() - 1].GetRectangle().getSize().x - 5.0f,
				m_shapes[m_shapes.size() - 1].GetRectangle().getSize().y));
		if (sizeLine > 5)
			m_count = m_count - Globals::UnitOfMeasure::minimizeLevelBar;
	}
}

std::vector<RectangleG>& FourthLevelScene::GetShapes()
{
	return m_shapes;
}

bool FourthLevelScene::IsInsideBackButton(const sf::Vector2f& position) const
{
	return m_buttons[2].ContainsPoint(position);
}

bool FourthLevelScene::IsInsideHelpButton(const sf::Vector2f& position) const
{
	return m_buttons[0].ContainsPoint(position);
}

bool FourthLevelScene::IsInsideRestartButton(const sf::Vector2f& position) const
{
	return m_buttons[1].ContainsPoint(position);
}

DrawCurvesG& FourthLevelScene::GetLine()
{
	return m_line;
}

int FourthLevelScene::GetNoParticlesInGlass() const
{
	uint16_t numberOfParticlesInsideTheGlass = 0;

	std::vector<b2Body*> particles = m_water.GetParticles();

	for (size_t index = 0; index < particles.size(); index++)
		if (particles[index]->GetPosition().x >= (2 + Globals::Glass::FourthPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y >= (8 + Globals::Glass::FourthPlatform::y / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().x >= (4.35 + Globals::Glass::FourthPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y <= (12 + Globals::Glass::FourthPlatform::y / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().x <= (6 + Globals::Glass::FourthPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y <= (12 + Globals::Glass::FourthPlatform::y / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().x <= (8.4 + Globals::Glass::FourthPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y >= (8 + Globals::Glass::FourthPlatform::y / Globals::UnitOfMeasure::scale))
			numberOfParticlesInsideTheGlass++;

	return numberOfParticlesInsideTheGlass;
}

void FourthLevelScene::SetLine(DrawCurvesG line)
{
	m_line = line;
}

bool FourthLevelScene::IsWinning() const
{
	VerifyPositionParticles();
	if (m_areaParticles > m_levelEditor.GetAreaGlass()* Globals::Glass::percentGlassNeeded)
		return true;
	return false;
}

b2World& FourthLevelScene::GetWorld()
{
	return m_world;
}

Water& FourthLevelScene::GetWater()
{
	return m_water;
}

void FourthLevelScene::Start()
{
	m_water.StartFourthLevel(dtClock, m_world);
}

void FourthLevelScene::VerifyPositionParticles() const
{
	constexpr float PI = Globals::Circle::PI;
	constexpr float radius = Globals::Circle::radius;
	uint16_t sum = 0;

	std::vector<b2Body*> particles = m_water.GetParticles();

	for (size_t index = 0; index < particles.size(); index++)
		if (particles[index]->GetPosition().x >= (2 + Globals::Glass::FourthPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y >= (8 + Globals::Glass::FourthPlatform::y / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().x >= (4.35 + Globals::Glass::FourthPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y <= (12 + Globals::Glass::FourthPlatform::y / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().x <= (6 + Globals::Glass::FourthPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y <= (12 + Globals::Glass::FourthPlatform::y / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().x <= (8.4 + Globals::Glass::FourthPlatform::x / Globals::UnitOfMeasure::scale)
			&& particles[index]->GetPosition().y >= (8 + Globals::Glass::FourthPlatform::y / Globals::UnitOfMeasure::scale))
			sum++;

	m_areaParticles = static_cast<float>(sum * PI * pow(radius, 2));
}