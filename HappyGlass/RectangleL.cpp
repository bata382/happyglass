#include "RectangleL.h"

RectangleL::RectangleL(const Point2DL& topLeftCorner, 
	const Point2DL& topRightCorner,
	const Point2DL& bottomLeftCorner,
	const Point2DL& bottomRightCorner):
	m_topLeftCorner(topLeftCorner),
	m_topRightCorner(topRightCorner),
	m_bottomLeftCorner(bottomLeftCorner),
	m_bottomRightCorner(bottomRightCorner)
{
}
