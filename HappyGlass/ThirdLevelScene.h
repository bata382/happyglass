#pragma once
#include <SFML/Graphics/RenderWindow.hpp>

#include "CircleG.h"
#include "DiamondG.h"
#include "DrawCurvesG.h"
#include "LevelEditor.h"
#include "RectangleG.h"
#include "Water.h"

class ThirdLevelScene
{
public:
	ThirdLevelScene() = default;
	ThirdLevelScene(sf::RenderWindow& window);
	ThirdLevelScene(const ThirdLevelScene& scene) = default;
	ThirdLevelScene(ThirdLevelScene&& scene) = default;
	ThirdLevelScene& operator=(const ThirdLevelScene& scene) = default;
	ThirdLevelScene& operator=(ThirdLevelScene&& scene) = default;
	~ThirdLevelScene() = default;

	void Draw();
	void Start();
	void ModifyLevelDrainBar(uint16_t sizeLine, uint16_t& size);

	std::vector<RectangleG>& GetShapes();
	DrawCurvesG& GetLine();
	b2World& GetWorld();
	Water& GetWater();
	int GetNoParticlesInGlass() const;

	void SetLine(DrawCurvesG line);

	bool IsInsideBackButton(const sf::Vector2f& position) const;
	bool IsInsideHelpButton(const sf::Vector2f& position) const;
	bool IsInsideRestartButton(const sf::Vector2f& position) const;

	bool IsWinning() const;

public:
	bool m_drawHintLine;
	bool m_click;
	uint16_t m_numberFrame;

private:
	void Generate();
	void VerifyPositionParticles() const;

private:
	float m_count;
	mutable float m_areaParticles;

	LevelEditor m_levelEditor;
	DrawCurvesG m_line;
	std::vector<RectangleG> m_buttons;
	std::vector<CircleG> m_waterParticles;
	std::vector<RectangleG> m_shapes;
	std::vector<DiamondG> m_diamonds;

	sf::RenderWindow& m_window;
	sf::Clock m_dtClock;

	/** Prepare the Box2D world */
	b2Vec2 m_gravity;
	b2World m_world;
	Water m_water;
};