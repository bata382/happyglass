#pragma once
#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>

#include "CircleG.h"
#include "DrawCurvesL.h"
#include "DrawCurvesG.h"
#include "RectangleG.h"
#include "SecondLevelScene.h"

#include <chrono>
#include <memory>
#include <vector>
#include <random>

class SecondLevel
{
public:
	SecondLevel();
	SecondLevel(const SecondLevel& scene) = default;
	SecondLevel(SecondLevel&& scene) = default;
	SecondLevel& operator=(const SecondLevel& scene) = default;
	SecondLevel& operator=(SecondLevel&& scene) = default;
	~SecondLevel() = default;

	void Run();
	sf::RenderWindow& GetWindow();

private:
	void InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground);
	void CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
		std::chrono::system_clock::time_point& lastFrame);
	void MakeAction(sf::Event& event, DrawCurvesG& line);

private:
	SecondLevelScene m_scene;
	DrawCurvesL m_lines;
	sf::RenderWindow  m_window;
};