#pragma once
#include <SFML/Graphics.hpp>

#include "Button.h"

#include <chrono>

class Application
{
public:
	~Application();

	void Run();
	static Application* GetInstance();

private:
	/*We need a private c-tor to prevent initialize more than one Application object*/
	Application();

	void InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground);
	void MakeAction(const sf::Event& event);
	void Draw();
	void CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
		std::chrono::system_clock::time_point& lastFrame);

private:
	enum class Menu
	{
		Menu,
		FirstLevel,
		SecondLevel,
		ThirdLevel,
		FourthLevel,
		Instructions,
		GaFirstLevel,
		GaSecondLevel,
		GaThirdLevel,
		GaFourthLevel
	};

private:
	sf::RenderWindow  m_window;
	/*Here will be stored the instance application*/
	static Application* m_application;
	/*Buuton options*/
	std::vector<RectangleG> m_buttons;
	/*Menu instance which tells where we are*/
	Menu m_menu;
};

