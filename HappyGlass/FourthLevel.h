#pragma once
#include <SFML/Graphics.hpp>

#include "DrawCurvesL.h"
#include "DrawCurvesG.h"
#include "FourthLevelScene.h"
#include "RectangleG.h"

#include <chrono>

class FourthLevel
{
public:
	FourthLevel();
	FourthLevel(const FourthLevel& scene) = default;
	FourthLevel(FourthLevel&& scene) = default;
	FourthLevel& operator=(const FourthLevel& scene) = default;
	FourthLevel& operator=(FourthLevel&& scene) = default;
	~FourthLevel() = default;

	void Run();
	sf::RenderWindow& GetWindow();

private:
	void InitializeWindow(sf::Texture& background, sf::Sprite& spriteBackground);
	void CalculateFramerate(std::chrono::system_clock::time_point& currentFrame,
		std::chrono::system_clock::time_point& lastFrame);
	void MakeAction(sf::Event& event, DrawCurvesG& line);

private:
	FourthLevelScene m_scene;
	DrawCurvesL m_lines;

	sf::RenderWindow  m_window;
};