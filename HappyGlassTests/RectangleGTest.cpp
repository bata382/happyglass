#include "CppUnitTest.h"
#include "..//HappyGlass/RectangleG.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace RectangleGTest
{
	TEST_CLASS(RectangleGTest)
	{
	public:
		TEST_METHOD(ConstructorTest)
		{
			RectangleG rectangle(sf::Vector2f(5, 5), sf::Vector2f(0, 0), sf::Color::Black);

			Assert::IsTrue(rectangle.GetRectangle().getSize() == sf::Vector2f(5, 5));
			Assert::IsTrue(rectangle.GetRectangle().getOrigin() == sf::Vector2f(0, 0));
			Assert::IsTrue(rectangle.GetRectangle().getFillColor() == sf::Color::Black);
		}

		TEST_METHOD(ContainsPointTest)
		{
			RectangleG rectangle(sf::Vector2f(5, 5), sf::Vector2f(0, 0), sf::Color::Black);

			Assert::AreEqual(true, rectangle.ContainsPoint(sf::Vector2f(1, 1)), (wchar_t*)("Rectangle doesn't contain point!"));
		}

		TEST_METHOD(DoesntContainPointTest)
		{
			RectangleG rectangle(sf::Vector2f(5, 5), sf::Vector2f(0, 0), sf::Color::Black);

			Assert::AreNotEqual(true, rectangle.ContainsPoint(sf::Vector2f(-5, -5)), (wchar_t*)("Rectangle contains point!"));
		}
	};
}
