#include "CppUnitTest.h"
#include "../HappyGlass/TrapezoidL.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TrapezoidTests
{
	TEST_CLASS(TrapezoidTests)
	{
	public:
		TEST_METHOD(ConstructorTest)
		{
			TrapezoidL trapezoid(Point2DL<uint16_t>(2, 7), Point2DL<uint16_t>(4, 7), Point2DL<uint16_t>(1, 2), Point2DL<uint16_t>(5, 2));

			Assert::IsTrue(trapezoid.GetTopLeftCorner().GetX() == 2 && trapezoid.GetTopLeftCorner().GetY() == 7);
			Assert::IsTrue(trapezoid.GetTopRightCorner().GetX() == 4 && trapezoid.GetTopRightCorner().GetY() == 7);
			Assert::IsTrue(trapezoid.GetBottomLeftCorner().GetX() == 1 && trapezoid.GetBottomLeftCorner().GetY() == 2);
			Assert::IsTrue(trapezoid.GetBottomRightCorner().GetX() == 5 && trapezoid.GetBottomRightCorner().GetY() == 2);
		}

		TEST_METHOD(PrincipalBaseTest)
		{
			TrapezoidL trapezoid(Point2DL<uint16_t>(2, 7), Point2DL<uint16_t>(4, 7), Point2DL<uint16_t>(1, 2), Point2DL<uint16_t>(5, 2));

			Assert::IsTrue(std::round(trapezoid.CalculatePrincipalBase() * 1000) == 4000);
			//Assert::AreSame(4000, (int)(std::round(trapezoid.CalculatePrincipalBase() * 1000)), (wchar_t*)("Principale Base is not correct claculated!"));
		}

		TEST_METHOD(SecondaryBaseTest)
		{
			TrapezoidL trapezoid(Point2DL<uint16_t>(2, 7), Point2DL<uint16_t>(4, 7), Point2DL<uint16_t>(1, 2), Point2DL<uint16_t>(5, 2));

			Assert::IsTrue(std::round(trapezoid.CalculateSecondaryBase() * 1000) == 2000);
		}

		TEST_METHOD(HeightTest)
		{
			TrapezoidL trapezoid(Point2DL<uint16_t>(2, 7), Point2DL<uint16_t>(4, 7), Point2DL<uint16_t>(1, 2), Point2DL<uint16_t>(5, 2));

			Assert::IsTrue(std::round(trapezoid.CalculateHeight() * 1000) == 5000);
		}

		TEST_METHOD(AreaTest)
		{
			TrapezoidL trapezoid(Point2DL<uint16_t>(2, 7), Point2DL<uint16_t>(4, 7), Point2DL<uint16_t>(1, 2), Point2DL<uint16_t>(5, 2));

			Assert::IsTrue(std::round(trapezoid.GetArea() * 1000) == 15000);
		}
	};
}
