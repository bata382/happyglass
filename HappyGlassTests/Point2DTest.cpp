#include "CppUnitTest.h"
#include "..//HappyGlass/Point2DL.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Point2DTest
{
	TEST_CLASS(Point2DTest)
	{
	public:
		TEST_METHOD(ConstructorTest)
		{
			Point2DL<int> point(3, 4);
			Assert::IsTrue(point.GetX() == 3);
			Assert::IsTrue(point.GetY() == 4);
		}

		TEST_METHOD(DistanceTest)
		{
			Point2DL<int> firstPoint(2, 3);
			Point2DL<int> secondPoint(4, 5);
			Assert::IsTrue(std::round(firstPoint.GetDistance(secondPoint) * 1000) == 2828);
		}
	};
}
