#include "CppUnitTest.h"
#include "../HappyGlass/DiamondG.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace DiamondGTests
{
	TEST_CLASS(DiamondGTests)
	{
	public:
		TEST_METHOD(ConstructorTest)
		{
			DiamondG diamond(sf::Vector2f(20, 30), 20);

			Assert::IsTrue(diamond.GetDiamond().getPosition() == sf::Vector2f(20, 30));
			Assert::IsTrue(diamond.GetDiamond().getRadius() == 20);
		}

		TEST_METHOD(ThreeStarsTest)
		{
			std::vector<DiamondG> diamonds;
			diamonds.emplace_back(sf::Vector2f(270, 270), 20);
			diamonds.emplace_back(sf::Vector2f(320, 270), 20);
			diamonds.emplace_back(sf::Vector2f(370, 270), 20);

			int count = 100;

			TransparentDiamonds(diamonds, count);

			Assert::IsFalse(diamonds[0].GetDiamond().getFillColor() == sf::Color::Transparent);
			Assert::IsFalse(diamonds[1].GetDiamond().getFillColor() == sf::Color::Transparent);
			Assert::IsFalse(diamonds[2].GetDiamond().getFillColor() == sf::Color::Transparent);
		}

		TEST_METHOD(TwoStarsTest)
		{
			std::vector<DiamondG> diamonds;
			diamonds.emplace_back(sf::Vector2f(270, 270), 20);
			diamonds.emplace_back(sf::Vector2f(320, 270), 20);
			diamonds.emplace_back(sf::Vector2f(370, 270), 20);

			int count = 65;

			TransparentDiamonds(diamonds, count);

			Assert::IsFalse(diamonds[0].GetDiamond().getFillColor() == sf::Color::Transparent);
			Assert::IsFalse(diamonds[1].GetDiamond().getFillColor() == sf::Color::Transparent);
			Assert::IsTrue(diamonds[2].GetDiamond().getFillColor() == sf::Color::Transparent);
		}

		TEST_METHOD(OneStarTest)
		{
			std::vector<DiamondG> diamonds;
			diamonds.emplace_back(sf::Vector2f(270, 270), 20);
			diamonds.emplace_back(sf::Vector2f(320, 270), 20);
			diamonds.emplace_back(sf::Vector2f(370, 270), 20);

			int count = 20;

			TransparentDiamonds(diamonds, count);

			Assert::IsFalse(diamonds[0].GetDiamond().getFillColor() == sf::Color::Transparent);
			Assert::IsTrue(diamonds[1].GetDiamond().getFillColor() == sf::Color::Transparent);
			Assert::IsTrue(diamonds[2].GetDiamond().getFillColor() == sf::Color::Transparent);
		}

	};
}
