#include "CppUnitTest.h"
#include "..//HappyGlass/DrawCurvesG.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace DrawCurvesGTest
{
	TEST_CLASS(DrawCurvesGTest)
	{
	public:
		TEST_METHOD(ConstructorTest)
		{
			DrawCurvesG line;

			Assert::AreEqual(false, line.m_locked, (wchar_t*)("Locked is not false!"));
			Assert::IsTrue(line.GetIsOkToDraw() == true);
			Assert::IsTrue(sf::Vector2f(-5, -25) == line.GetBorderOffset());
			Assert::IsTrue(line.GetSizeLine() == 0);
			Assert::IsTrue(sf::Vector2i(0, 0) == line.GetLastMousePos());
		}

		TEST_METHOD(AddPointTest)
		{
			DrawCurvesG line;
			line.AddFinalPoint();

			Assert::IsTrue(line.GetLinePoints().size() == 2);
		}
	};
}
