#include "CppUnitTest.h"
#include "../HappyGlass/CircleG.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CircleGTests
{
	TEST_CLASS(CircleGTests)
	{
	public:

		TEST_METHOD(ConstructorTest)
		{
			CircleG circle(20, sf::Color::Magenta, sf::Vector2f(100, 50));

			Assert::IsTrue(circle.GetCircle().getFillColor() == sf::Color::Magenta);
			Assert::IsTrue(circle.GetCircle().getRadius() == 20);
			Assert::IsTrue(circle.GetCircle().getPosition() == sf::Vector2f(100, 50));
		}

		TEST_METHOD(SetPositionTest)
		{
			CircleG circle(20, sf::Color::Magenta, sf::Vector2f(100, 50));

			circle.SetPosition(sf::Vector2f(50, 50));

			Assert::IsTrue(circle.GetCircle().getPosition() == sf::Vector2f(50, 50));
		}

	};
}
