#include "CppUnitTest.h"
#include "../HappyGlass/TextG.h"

#include <memory>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TextGTests
{
	TEST_CLASS(TextGTests)
	{
	public:

		TextG text;
		std::vector<std::unique_ptr<sf::Text>> texts = text.GenerateMenuTexts();
		TEST_METHOD(TextMenuGenerator)
		{
			int textSize = texts.size();
			Assert::IsTrue(textSize == 6);
		}

		TEST_METHOD(CorrectTextTest)
		{
			Assert::IsTrue(std::string(texts[0].get()->getString()).compare("Happy glass") == 0);
			Assert::IsTrue(std::string(texts[1].get()->getString()).compare("First level") == 0);
			Assert::IsTrue(std::string(texts[2].get()->getString()).compare("Second level") == 0);
			Assert::IsTrue(std::string(texts[3].get()->getString()).compare("Third level") == 0);
			Assert::IsTrue(std::string(texts[4].get()->getString()).compare("Fourth level") == 0);
			Assert::IsTrue(std::string(texts[5].get()->getString()).compare("Instructions") == 0);
		}

		TEST_METHOD(BackButtonTest)
		{
			sf::Text backButton = text.GenerateBackButtonText();

			Assert::IsTrue(backButton.getString().toAnsiString().compare("Back") == 0);
			Assert::IsTrue(backButton.getOutlineColor() == sf::Color::Black);
			Assert::IsTrue(backButton.getCharacterSize() == 30);
			Assert::IsTrue(backButton.getStyle() == sf::Text::Style::Bold);
			Assert::IsTrue(backButton.getFillColor() == sf::Color::Color(56, 217, 235));
			Assert::IsTrue(backButton.getPosition() == sf::Vector2f(928, 55));
		}

		TEST_METHOD(GenerateNumberTest)
		{
			sf::Text number = text.GenerateNumberText(15);

			Assert::IsTrue(number.getString() == "15");
			Assert::IsTrue(number.getCharacterSize() == 30);
			Assert::IsTrue(number.getOutlineColor() == sf::Color::Black);
			Assert::IsTrue(number.getStyle() == sf::Text::Style::Bold);
			Assert::IsTrue(number.getFillColor() == sf::Color::Color(56, 217, 235));
			Assert::IsTrue(number.getOutlineThickness() == 5);
			Assert::IsTrue(number.getPosition() == sf::Vector2f(175, 65));
		}

		TEST_METHOD(GenerateFinishText)
		{
			sf::Text finish = text.GenerateFinishText();

			Assert::IsTrue(finish.getCharacterSize() == 60);
			Assert::IsTrue(finish.getString().toAnsiString().compare("FINISH") == 0);
			Assert::IsTrue(finish.getFillColor() == sf::Color::Color(56, 217, 235));
			Assert::IsTrue(finish.getOutlineThickness() == 5);
			Assert::IsTrue(finish.getOutlineColor() == sf::Color::Black);
			Assert::IsTrue(finish.getPosition() == sf::Vector2f(425, 250));
		}

	};
}
